$(document).ready(function() {
  // Load modal to show intro text
  $("#intro_modal").modal({ backdrop: "static", keyboard: true });
  // Load users data
  const publicData = [];

  $("#MaidenName").attr({ readonly: true });

  $("#Sex").change(function() {
    let Sex = $(this).val();
    if (Sex == "F") {
      $("#MaidenName").attr({ readonly: false });
    } else {
      $("#MaidenName").attr({ readonly: true });
      $("#MaidenName").val("");
    }
  });

  // Set typeahead input field
  $(".twitter-typeahead").css("width : 100%");

  let TitleId = $("#TitleId").val();
  // Load Title
  $.ajax({
    type: "GET",
    url: "http://localhost/pension/json/SyncTitle.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#TitleId").append(
          $("<option>")
            .val(e.Id)
            .text(e.Value)
        );
      });
    }
  });

  // Load other pfa other_pfa
  $.ajax({
    type: "GET",
    url: "json/SyncOtherPFA.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $(".OtherPFA").append(
          $("<option>")
            .val(e.Id)
            .text(e.Name)
        );
      });
    }
  });

  // Nationality
  let nationality = $("#Nationality").val();
  $.ajax({
    type: "GET",
    url: "json/SyncCountry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#Nationality").append(
          $("<option>")
            .val(e.Id)
            .text(e.CountryName)
        );
      });
    }
  });

  $.ajax({
    type: "GET",
    url: "json/SyncCountry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#employer_country").append(
          $("<option>")
            .val(e.Id)
            .text(e.CountryName)
        );
      });
    }
  });

  // Load Industry
  $.ajax({
    type: "GET",
    url: "json/SyncIndustry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#Industry").append(
          $("<option>")
            .val(e.Id)
            .text(e.Title)
        );
      });
    }
  });

  // Make a eventlistner if Nationality is changed
  $("#Nationality").change(function() {
    let nationality = $(this).val();
    if ($(this).val() != "167") {
      $("#StateOfOrigin").html("<option value='Foreigner'>Foreigner</option>");
      $("#LGAOfOrigin").html("<option value='Foreigner'>Foreigner</option>");
    } else {
      $.ajax({
        type: "GET",
        url: "json/SyncState.json",
        success: function(html) {
          $("#StateOfOrigin").html("<option>State of Origin</option>");

          let result = JSON.parse(html);
          result.Data.forEach(function(e, i) {
            if (nationality == e.CountryID) {
              $("#StateOfOrigin").append(
                $("<option>")
                  .val(e.Id)
                  .text(e.StateName)
              );
            }
          });
        }
      });
    }
  });

  // Make a eventlistner if State is changed
  $("#StateOfOrigin").change(function() {
    let state = $(this).val();

    $.ajax({
      type: "GET",
      url: "json/SyncCity.json",
      success: function(html) {
        $("#LGAOfOrigin").html("<option>LGA of Origin</option>");

        let result = JSON.parse(html);
        result.Data.forEach(function(e, i) {
          if (state == e.StateID) {
            // console.log(e.StateID);
            $("#LGAOfOrigin").append(
              $("<option>")
                .val(e.Id)
                .text(e.LgaName)
            );
          }
        });
      }
    });
  });

  // Marrital Status
  $.ajax({
    type: "GET",
    url: "json/SyncMaritalStatus.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#MaritalStatus").append(
          $("<option>")
            .val(e.Id)
            .text(e.Description)
        );
      });
    }
  });

  // Select sector
  $.ajax({
    type: "GET",
    url: "json/SyncSector.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#SectorID").append(
          $("<option>")
            .val(e.SectorID)
            .text(e.SectorName)
        );
      });
    }
  });

  // If a sector was selected
  $("#SectorID").change(function() {
    let sector = $(this).val();
    $.ajax({
      type: "GET",
      url: "json/SyncSector.json",
      success: function(html) {
        if (sector == "2" || sector == "3") {
          $("#private_div").removeClass("hidden");
          $("#federal_state_div").addClass("hidden");
        } else if (sector == "1" || sector == "4" || sector == "5") {
          $("#private_div").addClass("hidden");
          $("#federal_state_div").removeClass("hidden");
        } else {
          $("#private_div").addClass("hidden");
          $("#federal_state_div").addClass("hidden");
        }
      }
    });
  });

  // Load Occupation
  $.ajax({
    type: "GET",
    url: "json/SyncOccupation.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#occupation").append(
          $("<option>")
            .val(e.Id)
            .text(e.Title)
        );
      });
    }
  });

  // Load country of Residenet for Employer
  $.ajax({
    type: "GET",
    url: "json/SyncCountry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#country_sector").append(
          $("<option>")
            .val(e.Id)
            .text(e.CountryName)
        );
      });
    }
  });

  // Make a eventlistner if Nationality is changed
  $("#CityCode").change(function() {
    let country_res = $(this).val();

    if ($(this).val() != "167") {
      $("#StateCode").html("<option value='Foreigner'>Foreigner</option>");
      $("#lga_res").html("<option value='Foreigner'>Foreigner</option>");
    } else {
      $.ajax({
        type: "GET",
        url: "json/SyncState.json",
        success: function(html) {
          // console.log(html.Data);
          let result = html.Data;
          $("#StateCode").html("<option>State of Origin</option>");

          result.forEach(function(e, i) {
            if (country_res == e.CountryID) {
              $("#StateCode").append(
                $("<option>")
                  .val(e.Id)
                  .text(e.StateName)
              );
            }
          });
        }
      });
    }
  });

  // Make a eventlistner if State is changed
  $("#StateCode").change(function() {
    let state_res = $(this).val();

    $.ajax({
      type: "GET",
      url: "json/SyncCity.json",
      success: function(html) {
        // console.log(html.Data);
        let result = html.Data;
        $("#lga_res").html("<option>LGA of Origin</option>");

        result.forEach(function(e, i) {
          if (state_res == e.StateID) {
            // console.log(e.StateID);
            $("#lga_res").append(
              $("<option>")
                .val(e.Id)
                .text(e.LgaName)
            );
          }
        });
      }
    });
  });

  // Fetch Qualification qualification
  $.ajax({
    type: "GET",
    url: "json/SyncQualification.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#Qualification").append(
          $("<option>")
            .val(e.Id)
            .text(e.Title)
        );
      });
    }
  });

  // Fetch Industry
  $.ajax({
    type: "GET",
    url: "json/SyncIndustry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#Industry").append(
          $("<option>")
            .val(e.Id)
            .text(e.Title)
        );
      });
    }
  });

  // Load organisation org_name
  // $.ajax({
  //   type: "GET",
  //   url: "json/SyncEmployer.json",
  //   success: function(html) {
  //     let title = html.Data;

  //     title.forEach(function(e, i) {
  //       $("#org_name").append(
  //         $("<option>")
  //           .val(e.Id)
  //           .text(e.EmployerName)
  //       );
  //     });
  //   }
  // });

  // $("#org_name").on("change", function(e) {
  //   console.log($(e.target).val());
  //   let org_name = $(this).val();
  //   $.ajax({
  //     type: "GET",
  //     url: "json/SyncEmployer.json",
  //     success: function(html) {
  //       let title = html.Data;

  //       title.forEach(function(e, i) {
  //         if (org_name == e.EmployerName) {
  //           $("#emp_code").val(e.EmployerCode);
  //         }
  //       });
  //     }
  //   });
  // });

  $("input#org_name").on("change", function(e) {
    // console.log($(e.target).val());
    let org_name = $(this).val();
    $.ajax({
      type: "GET",
      url: "json/SyncEmployer.json",
      success: function(html) {
        let title = html.Data;

        title.forEach(function(e, i) {
          if (org_name == e.EmployerName) {
            $("#EmployerCode").val(e.EmployerCode);
            $("#EmployerId").val(e.Id);
          }
        });
      }
    });
  });

  $("#retirement_date input").datepicker({
    format: "yyyy-mm-dd",
    todayBtn: "linked",
    todayHighlight: true,
    autoclose: true
  });

  let counter = 2;

  $("#add_btn").click(function(e) {
    e.preventDefault();
    // alert("hello")

    let fieldHtml =
      '<div class="form-row mb-5" id="TextBoxDiv' +
      counter +
      '">' +
      '<div class="col-md-3" >' +
      '<label for="">Salary Structure</label>' +
      '<select name="salary_structure[]" id="salary_structure" class="form-control salary_structure">' +
      "<option>Select An Salary Structure</option>" +
      '<option value="Harmonized Salary Structure_2014">Harmonized Salary Structure As At 2014 (2014)</option>' +
      '<option value="Consolidated Salary_2007">Consolidated Salary As At 2007 (2007)</option>' +
      '<option value="Consolidated Salary_2010">Consolidated Salary As At 2010 (2010)</option>' +
      '<option value="Harmonized Salary Structure_2013">Harmonized Salary Structure As At 2013 (2013) </option>' +
      '<option value="Harmonized Salary Structure_2016">Harmonized Salary Structure As At 2016 (2016) </option>' +
      '<option value="Current Salary Structure_2019">Current Salary Structure (2019)</option>' +
      "</select>" +
      "</div >" +
      '<div class="col-md-3">' +
      '<label for="">Grade Level</label>' +
      '<input type="text" class="form-control grade_level" name="grade_level[]" id="grade_level" placeholder="Grade Level" />' +
      "</div>" +
      '<div class="col-md-3">' +
      '<label for="">Grade Step</label>' +
      '<input type="text" class="form-control grade_step" name="grade_step[]" id="grade_step" placeholder="Grade Step" />' +
      "</div>" +
      '<div class="col-md-3">' +
      '<label for="">Salary</label>' +
      '<input type="number" class="form-control salary" name="salary[]" id="salary" placeholder="Salary" />' +
      "</div>" +
      "</div >";

    $("#fieldwrapper").append(fieldHtml);
    counter++;
  });

  $("#clear_btn").click(function() {
    if (counter == 1) {
      // alert("No more textbox to remove");
      return false;
    }

    counter--;

    $("#TextBoxDiv" + counter).remove();
  });

  let otherpfacounter = 2;

  $("#add_other_pfa").click(function(e) {
    e.preventDefault();
    let fieldHtml =
      '<div class="form-row mb-5" id="TextBoxDiv_Otherpfa' +
      otherpfacounter +
      '">' +
      '<div class="col-md-6" >' +
      '<label for="">Salary Structure</label>' +
      '<select name="OtherPFA[]" id="OtherPFA" class="form-control OtherPFA">' +
      "</select>" +
      "</div >" +
      '<div class="col-md-6">' +
      '<label for="">Other Pencom PIN</label>' +
      '<input type="text" class="form-control OtherPencomPIN" name="OtherPencomPIN[]" id="OtherPencomPIN" placeholder="Other Pencom PIN" />' +
      "</div>" +
      "</div >";

    $("#fieldwrapper_pfa").append(fieldHtml);

    $.ajax({
      type: "GET",
      url: "json/SyncOtherPFA.json",
      success: function(html) {
        let title = html.Data;
        title.forEach(function(e, i) {
          $(".OtherPFA").append(
            $("<option>")
              .val(e.Id)
              .text(e.Name)
          );
        });
      }
    });

    otherpfacounter++;
  });

  $("#clear_other_pfa").click(function() {
    if (counter == 1) {
      return false;
    }

    otherpfacounter--;

    $("#TextBoxDiv_Otherpfa" + otherpfacounter).remove();
  });

  // $("#add_btn").click(function(e) {
  //   e.preventDefault();
  //   $("#dynamic_table").removeClass("hidden");

  //   let salary_structure = $("#salary_structure").val();
  //   salary_structure = salary_structure.split("_");
  //   let publicObj = {};
  //   if (
  //     salary_structure[0] &&
  //     salary_structure[1] &&
  //     $("#grade_level").val() &&
  //     $("#grade_step").val() &&
  //     $("#salary").val()
  //   ) {
  //     let today = new Date();
  //     let date =
  //       today.getFullYear() +
  //       "-" +
  //       (today.getMonth() + 1) +
  //       "-" +
  //       today.getDate();
  //     let time =
  //       today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  //     let dateTime = date + " " + time;

  //     publicObj = {
  //       SalaryStructure: salary_structure[0],
  //       GL: $("#grade_level").val(),
  //       Year: salary_structure[1],
  //       Salary: $("#salary").val(),
  //       DateAdded: dateTime
  //     };

  //     publicData.push(publicObj);

  //     $("#dynamic_table tbody").append(
  //       "<tr><td>" +
  //         salary_structure[0] +
  //         "</td><td>" +
  //         salary_structure[1] +
  //         "</td><td>" +
  //         $("#grade_level").val() +
  //         "</td><td>" +
  //         $("#grade_step").val() +
  //         "</td><td>" +
  //         $("#salary").val() +
  //         "</td></tr>"
  //     );
  //     $("#grade_level").val("");
  //     $("#grade_step").val("");
  //     $("#salary").val("");
  //   }
  // });

  $("#clear_btn").click(function(e) {
    e.preventDefault();
    $("#dynamic_table").addClass("hidden");
    $("#dynamic_table tbody").html("");
  });

  // Load Employers Country Code
  // Load country of Residenet for Employer
  $.ajax({
    type: "GET",
    url: "json/SyncCountry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#CityCode").append(
          $("<option>")
            .val(e.Id)
            .text(e.CountryName)
        );
      });
    }
  });

  // Make a eventlistner if Nationality is changed
  $.ajax({
    type: "GET",
    url: "json/SyncCountry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#country_res_code").append(
          $("<option>")
            .val(e.Id)
            .text(e.CountryName)
        );
      });
    }
  });

  $("#country_res_code").change(function() {
    let country_res_code = $(this).val();

    if ($(this).val() != "167") {
      $("#state_res_code").html("<option value='Foreigner'>Foreigner</option>");
      $("#lga_res_code").html("<option value='Foreigner'>Foreigner</option>");
    } else {
      $.ajax({
        type: "GET",
        url: "json/SyncState.json",
        success: function(html) {
          // console.log(html.Data);
          let result = html.Data;
          $("#state_res_code").html("<option>State of Origin</option>");

          result.forEach(function(e, i) {
            if (country_res_code == e.CountryID) {
              $("#state_res_code").append(
                $("<option>")
                  .val(e.Id)
                  .text(e.StateName)
              );
            }
          });
        }
      });
    }
  });

  // Make a eventlistner if State is changed
  $("#state_res_code").change(function() {
    let state_res_code = $(this).val();

    $.ajax({
      type: "GET",
      url: "json/SyncCity.json",
      success: function(html) {
        // console.log(html.Data);
        let result = html.Data;
        $("#lga_res_code").html("<option>LGA of Origin</option>");

        result.forEach(function(e, i) {
          if (state_res_code == e.StateID) {
            // console.log(e.StateID);
            $("#lga_res_code").append(
              $("<option>")
                .val(e.Id)
                .text(e.LgaName)
            );
          }
        });
      }
    });
  });

  // Load ID card Type
  $.ajax({
    type: "GET",
    url: "json/SyncIDType.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#id_card_type").append(
          $("<option>")
            .val(e.Id)
            .text(e.IDType)
        );
      });
    }
  });

  // Load Title for next of Kin
  $.ajax({
    type: "GET",
    url: "json/SyncTitle.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#NOKTitle").append(
          $("<option>")
            .val(e.Id)
            .text(e.Value)
        );
      });
    }
  });

  // Load relationship for Next of Kin
  $.ajax({
    type: "GET",
    url: "json/SyncRelationship.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#NOKRelationship").append(
          $("<option>")
            .val(e.Id)
            .text(e.Value)
        );
      });
    }
  });

  // Load country state and LGA of Next of Kin

  $.ajax({
    type: "GET",
    url: "json/SyncCountry.json",
    success: function(html) {
      let title = JSON.parse(html);
      title.Data.forEach(function(e, i) {
        $("#NOKCountryCode").append(
          $("<option>")
            .val(e.Id)
            .text(e.CountryName)
        );
      });
    }
  });

  // Make a eventlistner if Nationality is changed
  $("#NOKCountryCode").change(function() {
    let NOKCountryCode = $(this).val();

    if ($(this).val() != "167") {
      $("#NOKStateCode").html("<option value='Foreigner'>Foreigner</option>");
      $("#NOKCityCode").html("<option value='Foreigner'>Foreigner</option>");
    } else {
      $.ajax({
        type: "GET",
        url: "json/SyncState.json",
        success: function(html) {
          // console.log(html.Data);
          let result = html.Data;
          $("#NOKStateCode").html("<option>State of Origin</option>");

          result.forEach(function(e, i) {
            if (NOKCountryCode == e.CountryID) {
              $("#NOKStateCode").append(
                $("<option>")
                  .val(e.Id)
                  .text(e.StateName)
              );
            }
          });
        }
      });
    }
  });

  // Make a eventlistner if State is changed
  $("#NOKStateCode").change(function() {
    let nxt_kin_state = $(this).val();

    $.ajax({
      type: "GET",
      url: "json/SyncCity.json",
      success: function(html) {
        // console.log(html.Data);
        let result = html.Data;
        $("#NOKCityCode").html("<option>LGA of Origin</option>");

        result.forEach(function(e, i) {
          if (nxt_kin_state == e.StateID) {
            // console.log(e.StateID);
            $("#NOKCityCode").append(
              $("<option>")
                .val(e.Id)
                .text(e.LgaName)
            );
          }
        });
      }
    });
  });

  $("#customer_ippis").change(function() {
    let customer_ippis = $(this).val();
    if (customer_ippis == "Yes") {
      $("#ippis_div").removeClass("hidden");
    } else {
      $("#ippis_div").addClass("hidden");
    }
  });

  // Load Place of Birth
  placeOfBirth();

  // Load Organisation
  fetchOrganisation();

  $(".datepicker").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    maxDate: "0d",
    changeMonth: true,
    changeYear: true
  });

  // Disable Submit button
  $("#pension_submit").prop("disabled", true);
  $("#agree_form").click(function() {
    swal({
      title: "Notification!",
      text:
        "Please ensure you filled correctly all the data capture information...",
      icon: "success",
      button: "Close"
    });
    $("#pension_submit").prop("disabled", false);
  });

  // Submit the for here
  // submitForm();

  // Load Users details if it exist
  verifyUser();

  // Load modal for otherPFA
  $("#associated_pin").click(function() {
    if ($(this).is(":checked")) {
      // $("#modal-associated-pins").show();
      $("#modal-associated-pins").modal({ backdrop: "static", keyboard: true });
    } else {
      $("#modal-associated-pins").hide();
    }
  });
});

function placeOfBirth() {
  $.ajax({
    type: "GET",
    url: "json/SyncCity.json",
    success: function(html) {
      let cities = [];

      let result = JSON.parse(html);
      result.Data.forEach(function(e, i) {
        cities.push(e.LgaName);
      });

      var states = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: cities
      });

      $(".typeahead").typeahead(
        {
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
          name: "states",
          source: states,
          limit: Number.MAX_VALUE, // Set the limit as high as possible.
          templates: {
            empty: ['<div class="empty-message">No record found </div>']
          }
        }
      );
    }
  });
}

function fetchOrganisation() {
  $.ajax({
    type: "GET",
    url: "json/SyncEmployer.json",
    success: function(html) {

      let cities = [];

      let result = JSON.parse(html);
      result.Data.forEach(function(e, i) {
        cities.push(e.EmployerName);
      });

      var states = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: cities
      });

      $(".org_name").typeahead(
        {
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
          name: "states",
          source: states,
          limit: Number.MAX_VALUE, // Set the limit as high as possible.
          templates: {
            empty: ['<div class="empty-message">No record found </div>']
          }
        }
      );
    }
  });
}

function verifyUser() {
  $.ajax({
    type: "POST",
    headers: { AppKey: "54321" },
    url:
      "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/GetTempCustomerDataUpdate?rsapin=" +
      pin,
    // url: "json/payload.json",
    beforeSend: function() {
      $.blockUI({
        css: {
          border: "none",
          padding: "15px",
          backgroundColor: "#000",
          "-webkit-border-radius": "10px",
          "-moz-border-radius": "10px",
          opacity: 0.5,
          color: "#fff"
        }
      });
    },
    success: function(response) {
      $.unblockUI();
      console.log(response);
      if (response.StatusCode == "00") {
        localStorage.setItem("Select1", response.Data.TitleId);
        $("#TitleId")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select1")) {
              $("#TitleId").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select2", response.Data.OtherPFA);
        $("#OtherPFA")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select2")) {
              $("#OtherPFA").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select3", response.Data.SectorID);
        $("#SectorID")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select3")) {
              $("#SectorID").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select4", response.Data.Sex);
        $("#Sex")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select4")) {
              $("#Sex").prop("selectedIndex", i);
            }
            if (Sex == "F") {
              $("#MaidenName").attr({ readonly: false });
            } else {
              $("#MaidenName").attr({ readonly: true });
              $("#MaidenName").val("");
            }
          });

        localStorage.setItem("Select5", response.Data.NOKTitle);
        $("#NOKTitle")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select5")) {
              $("#NOKTitle").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select6", response.Data.NOKRelationship);
        $("#NOKRelationship")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select6")) {
              $("#NOKRelationship").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select7", response.Data.NOKSex);
        $("#NOKSex")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select7")) {
              $("#NOKSex").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select8", response.Data.EmployerCountry);
        $("#EmployerCountry")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select8")) {
              $("#EmployerCountry").prop("selectedIndex", i);
            }
          });
        localStorage.setItem("Select9", response.Data.SectorID);
        $("#EmployerStateCode")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select9")) {
              $("#EmployerStateCode").prop("selectedIndex", i);
            }
          });
        localStorage.setItem("Select10", response.Data.SectorID);
        $("#EmployerCityCode")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select10")) {
              $("#EmployerCityCode").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select11", response.Data.Nationality);
        $("#Nationality")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select11")) {
              $("#Nationality").prop("selectedIndex", i);
              if ($(this).val() != "167") {
                $("#StateOfOrigin").html(
                  "<option value='Foreigner'>Foreigner</option>"
                );
                $("#LGAOfOrigin").html(
                  "<option value='Foreigner'>Foreigner</option>"
                );
              } else {
                // StateOfOrigin
                $.ajax({
                  type: "GET",
                  url: "json/SyncState.json",
                  success: function(html) {
                    // console.log(html.Data);
                    let result = html.Data;
                    // $("#StateOfOrigin").html("<option>State of Origin</option>");

                    result.forEach(function(e, i) {
                      if (response.Data.Nationality == e.CountryID) {
                        $("#StateOfOrigin").append(
                          $("<option>")
                            .val(e.Id)
                            .text(e.StateName)
                        );
                      }
                    });

                    $("#StateOfOrigin")
                      .find("option")
                      .each(function(i, e) {
                        if ($(e).val() == response.Data.StateOfOrigin) {
                          $("#StateOfOrigin").prop("selectedIndex", i);
                        }
                      });

                    $.ajax({
                      type: "GET",
                      url: "json/SyncCity.json",
                      success: function(html) {
                        let result = html.Data;
                        result.forEach(function(e, i) {
                          if (response.Data.StateOfOrigin == e.StateID) {
                            $("#LGAOfOrigin").append(
                              $("<option>")
                                .val(e.Id)
                                .text(e.LgaName)
                            );
                          }
                        });

                        $("#LGAOfOrigin")
                          .find("option")
                          .each(function(i, e) {
                            if ($(e).val() == response.Data.LGAOfOrigin) {
                              $("#LGAOfOrigin").prop("selectedIndex", i);
                            }
                          });
                      }
                    });
                  }
                });
              }
            }
          });

        localStorage.setItem("Select12", response.Data.Qualification);
        $("#Qualification")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select12")) {
              $("#Qualification").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select13", response.Data.Occupation);
        $("#Occupation")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select13")) {
              $("#Occupation").prop("selectedIndex", i);
            }
          });

        localStorage.setItem("Select14", response.Data.NOKCountryCode);
        $("#NOKCountryCode")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select14")) {
              $("#NOKCountryCode").prop("selectedIndex", i);
              if ($(this).val() != "167") {
                $("#NOKStateCode").html(
                  "<option value='Foreigner'>Foreigner</option>"
                );
                $("#NOKCityCode").html(
                  "<option value='Foreigner'>Foreigner</option>"
                );
              } else {
                // NOKStateCode
                $.ajax({
                  type: "GET",
                  url: "json/SyncState.json",
                  success: function(html) {
                    // console.log(html.Data);
                    let result = html.Data;
                    // $("#NOKStateCode").html("<option>State of Origin</option>");

                    result.forEach(function(e, i) {
                      if (response.Data.Nationality == e.CountryID) {
                        $("#NOKStateCode").append(
                          $("<option>")
                            .val(e.Id)
                            .text(e.StateName)
                        );
                      }
                    });

                    $("#NOKStateCode")
                      .find("option")
                      .each(function(i, e) {
                        if ($(e).val() == response.Data.NOKStateCode) {
                          $("#NOKStateCode").prop("selectedIndex", i);
                        }
                      });

                    $.ajax({
                      type: "GET",
                      url: "json/SyncCity.json",
                      success: function(html) {
                        let result = html.Data;
                        result.forEach(function(e, i) {
                          if (response.Data.NOKStateCode == e.StateID) {
                            $("#NOKCityCode").append(
                              $("<option>")
                                .val(e.Id)
                                .text(e.LgaName)
                            );
                          }
                        });

                        $("#NOKCityCode")
                          .find("option")
                          .each(function(i, e) {
                            if ($(e).val() == response.Data.NOKCityCode) {
                              $("#NOKCityCode").prop("selectedIndex", i);
                            }
                          });
                      }
                    });
                  }
                });
              }
            }
          });

        localStorage.setItem("Select14", response.Data.Industry);
        $("#Industry")
          .find("option")
          .each(function(i, e) {
            if ($(e).val() == localStorage.getItem("Select14")) {
              $("#Industry").prop("selectedIndex", i);
            }
          });

        $("#BVN").val(response.Data.BVN);
        $("#NIN").val(response.Data.NIN);
        $("#DateOfBirth").val(response.Data.DateOfBirth);
        $("#Email").val(
          response.Data.Email ? response.Data.Email : "info@leadway-pensure.com"
        );
        $("#EmployerVillageTownCity").val(
          response.Data.EmployerVillageTownCity
        );
        $("#AgentCode").val(
          response.Data.AgentCode ? response.Data.AgentCode : "000"
        );
        $("#LPPFAUsername").val(
          response.Data.LPPFAUsername ? response.Data.LPPFAUsername : "o-elegah"
        );
        $("#FirstName").val(response.Data.FirstName);
        $("#LastName").val(response.Data.LastName);
        $("#OtherName").val(response.Data.OtherName);
        $("#OtherPencomPIN").val(response.Data.OtherPencomPIN);
        $("#PlaceOfBirth").val(response.Data.PlaceOfBirth);
        $("#ServiceNo").val(response.Data.ServiceNo);
        $("#ServiceId").val(response.Data.ServiceId);
        $("#StreetName").val(response.Data.StreetName);
        $("#TelephoneNo").val(response.Data.TelephoneNo);
        $("#UserId").val(response.Data.UserId);
        $("#VillageTownCity").val(response.Data.VillageTownCity);
        $("#ZipCode").val(response.Data.ZipCode);
        $("#RSAPIN").val(
          // "PEN" +
          //   Math.random()
          //     .toString()
          //     .substr(2, 12)
          response.Data.RSAPIN
        );
        $("#DateOfCurrentEmployment").val(
          response.Data.DateOfCurrentEmployment
        );
        $("#NOKLastName").val(response.Data.NOKLastName);
        $("#NOKFirstName").val(response.Data.NOKFirstName);
        $("#NOKOtherNames").val(response.Data.NOKOtherNames);
        $("#NOKPhone").val(response.Data.NOKPhone);
        $("#NOKMobile").val(response.Data.NOKMobile);
        $("#NOKStreetName").val(response.Data.NOKStreetName);
        $("#NOKVillageTownCity").val(response.Data.NOKVillageTownCity);
        $("#NOKZipCode").val(response.Data.NOKZipCode);
        $("#OfficeTelNo").val(response.Data.OfficeTelNo);
        $("#OfficialEmailAddress").val(response.Data.OfficialEmailAddress);
        $("#ExpectedEEMonthlyContr").val(response.Data.ExpectedEEMonthlyContr);
        $("#ExpectedERMonthlyContr").val(response.Data.ExpectedERMonthlyContr);
        $("#EmployerId").val(response.Data.EmployerId);
        $("#EmployerCode").val(response.Data.EmployerCode);
        $("#EmployerBuildingNumberName").val(
          response.Data.EmployerBuildingNumberName
        );

        if (response.Data.PublicSectorEmploymentSalaryData.length > 0) {
          $("#TextBoxDiv1").remove();
          counter = 2;

          // $("#private_div").addClass("hidden");
          $("#federal_state_div").removeClass("hidden");
          // $("#dynamic_table").removeClass("hidden");
          $("#fieldwrapper").html("");
          let fieldHtml = "";

          for (let e of response.Data.PublicSectorEmploymentSalaryData) {
            fieldHtml =
              '<div class="form-group mb-5 row" id="TextBoxDiv' +
              counter +
              '">' +
              '<div class="col-md-3" >' +
              '<label for="">Salary Structure</label>' +
              '<select name="salary_structure[]" id="salary_structure"' +
              '" class="form-control salary_structure salary_tag' +
              counter +
              '">' +
              "<option>Select An Salary Structure</option>" +
              '<option value="Harmonized Salary Structure_2014">Harmonized Salary Structure As At 2014 (2014)</option>' +
              '<option value="Consolidated Salary_2007">Consolidated Salary As At 2007 (2007)</option>' +
              '<option value="Consolidated Salary_2010">Consolidated Salary As At 2010 (2010)</option>' +
              '<option value="Harmonized Salary Structure_2013">Harmonized Salary Structure As At 2013 (2013) </option>' +
              '<option value="Harmonized Salary Structure_2016">Harmonized Salary Structure As At 2016 (2016) </option>' +
              '<option value="Current Salary Structure_2019">Current Salary Structure (2019)</option>' +
              "</select>" +
              "</div >" +
              '<div class="col-md-3">' +
              '<label for="">Grade Level</label>' +
              '<input type="text" class="form-control grade_level" name="grade_level[]" value="' +
              e.GL +
              ' " id="grade_level" placeholder="Grade Level" />' +
              "</div>" +
              '<div class="col-md-3">' +
              '<label for="">Grade Step</label>' +
              '<input type="text" class="form-control grade_step" name="grade_step[]" value="' +
              e.Step +
              '" id="grade_step" placeholder="Grade Step" />' +
              "</div>" +
              '<div class="col-md-3">' +
              '<label for="">Salary</label>' +
              '<input type="number" class="form-control salary" name="salary[]" value="' +
              e.Salary +
              '" id="salary" placeholder="Salary" />' +
              "</div>" +
              "</div >";

            $("#fieldwrapper").append(fieldHtml);

            localStorage.setItem("Select15" + counter, e.SalaryStructure);
            $(".salary_tag" + counter)
              .find("option")
              .each(function(i, j) {
                // alert($(j).val());
                if ($(j).val() == localStorage.getItem("Select15" + counter)) {
                  $(".salary_tag" + counter).prop("selectedIndex", i);
                }
              });

            console.log(localStorage.getItem("Select15" + counter));

            counter++;
          }

          // $("#add_btn").click(function (e) {
          //   e.preventDefault();
          //   $("#fieldwrapper").append(fieldHtml);
          //   counter++;
          // });

          $("#clear_btn").click(function() {
            if (counter == 1) {
              // alert("No more textbox to remove");
              return false;
            }

            counter--;

            $("#TextBoxDiv" + counter).remove();
          });
        }
      }
    }
  });
}

function uploadPPP(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);
      var uploadPic = document.getElementById("uploadPic");
      var file = uploadPic.files[0];

      if (file.size / 100 > 500) {
        $("#uploadPic").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else {
        $("#uploadPic").removeClass("input-error");

        $("#profile_pic_ppp").val(
          e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
        );
      }
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function uploadMeansOfID(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var MeansOfID = document.getElementById("MeansOfID");
      var file = MeansOfID.files[0];

      if (file.size / 1000 > 500) {
        $("#MeansOfID").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else {
        $("#MeansOfID").removeClass("input-error");

        $("#txtMeansOfID").val(
          e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
        );
      }
    };
    reader.readAsDataURL(input.files[0]);
  }
}
function uploadProofOfResidency(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var ProofOfResidency = document.getElementById("ProofOfResidency");
      var file = ProofOfResidency.files[0];

      if (file.size / 1000 > 500) {
        $("#ProofOfResidency").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else {
        $("#ProofOfResidency").removeClass("input-error");

        $("#txtProofOfResidency").val(
          e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
        );
      }
    };
    reader.readAsDataURL(input.files[0]);
  }
}
function uploadPhyChallengeDocument(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var PhyChallengeDocument = document.getElementById(
        "PhyChallengeDocument"
      );
      var file = PhyChallengeDocument.files[0];

      if (file.size / 1000 > 500) {
        $(this).addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else $(this).removeClass("input-error");

      $("#txtPhyChallengeDocument").val(
        e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
      );
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function uploadSignature(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var uploadSignatureFile = document.getElementById("uploadSignatureFile");
      var file = uploadSignatureFile.files[0];

      if (file.size / 1000 > 500) {
        $(this).addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else {
        $("#uploadSignatureFile").removeClass("input-error");
        $("#txtSignature").val(
          e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
        );
      }
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function uploadIdCard(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var uploadIdCardFile = document.getElementById("uploadIdCardFile");
      var file = uploadIdCardFile.files[0];

      if (file.size / 1000 > 500) {
        $(this).addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else {
        $("#uploadIdCardFile").removeClass("input-error");
        $("#txtidCard").val(
          e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
        );
      }
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function uploadUtilityBill(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var uploadUtilityBillFile = document.getElementById(
        "uploadUtilityBillFile"
      );
      var file = uploadUtilityBillFile.files[0];

      if (file.size / 1000 > 500) {
        $("#uploadUtilityBillFile").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else $("#uploadUtilityBillFile").removeClass("input-error");

      $("#txtBirthCertificate").val(
        e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
      );
    };
    reader.readAsDataURL(input.files[0]);
  }
}
function uploadDoc1(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var uploadDoc1 = document.getElementById("uploadDoc1");
      var file = uploadDoc1.files[0];

      if (file.size / 1000 > 500) {
        $("#uploadDoc1").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else $("#uploadDoc1").removeClass("input-error");

      $("#txtDoc1").val(
        e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
      );
    };
    reader.readAsDataURL(input.files[0]);
  }
}
function uploadDoc2(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var uploadDoc2 = document.getElementById("uploadDoc2");
      var file = uploadDoc2.files[0];

      if (file.size / 1000 > 500) {
        $("#uploadDoc2").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else $("#uploadDoc2").removeClass("input-error");

      $("#txtDoc2").val(
        e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
      );
    };
    reader.readAsDataURL(input.files[0]);
  }
}
function uploadDoc3(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var uploadDoc3 = document.getElementById("uploadDoc3");
      var file = uploadDoc3.files[0];

      if (file.size / 1000 > 500) {
        $("#uploadDoc3").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else $("#uploadDoc3").removeClass("input-error");

      $("#txtDoc3").val(
        e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
      );
    };
    reader.readAsDataURL(input.files[0]);
  }
}
function uploadDoc4(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#falseinput").attr("src", e.target.result);

      var uploadDoc4 = document.getElementById("uploadDoc4");
      var file = uploadDoc4.files[0];

      if (file.size / 1000 > 500) {
        $("#uploadDoc4").addClass("input-error");
        swal("Notification", "File to large not more than 500kb", "error");
      } else $("#uploadDoc4").removeClass("input-error");

      $("#txtDoc4").val(
        e.target.result.replace(/^data:image\/(png|jpg);base64,/, "")
      );
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function scroll_to_class(element_class, removed_height) {
  var scroll_to = $(element_class).offset().top - removed_height;
  if ($(window).scrollTop() != scroll_to) {
    $("html, body")
      .stop()
      .animate({ scrollTop: scroll_to }, 0);
  }
}

function bar_progress(progress_line_object, direction) {
  var number_of_steps = progress_line_object.data("number-of-steps");
  var now_value = progress_line_object.data("now-value");
  var new_value = 0;
  if (direction == "right") {
    new_value = now_value + 100 / number_of_steps;
  } else if (direction == "left") {
    new_value = now_value - 100 / number_of_steps;
  }
  progress_line_object
    .attr("style", "width: " + new_value + "%;")
    .data("now-value", new_value);
}

function saveData() {
  if($("#BVN").val() == "" && $("#NIN").val() == "" ){
    swal("Notification", "Please fill either your BVN or NIN number", "error");
    return;
  }
  else if (
      $("#TitleId").val() == "" ||
      $("#Sex").val() == "" ||
      $("#LastName").val() == "" ||
      $("#check_error").val() == "" ||
      $("#DateOfBirth").val() == "" ||
      $("#Nationality").val() == "" ||
      $("#StateOfOrigin").val() == "" ||
      $("#LGAOfOrigin").val() == "" ||
      $("#physically_challenged").val() == ""
  ) {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};
    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val() ? $("#BVN").val() : '00000000000',
      NIN: $("#NIN").val() ? $("#NIN").val() : '00000000000',
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("#review_div").html("");
        // reviewData();

        var href = "<a href='/Kingsley_Resume.pdf'>Download</a>";

        if (response.StatusCode == "00") {
          // swal("Good job!", href, "success");
          swal({
            html: true,
            title: "Good job!",
            text: '<a target="_blank" href="Kingsley Resume.pdf">Download</a>'
          });
        }
      }
    });
  }
}

function saveDataLater() {
  if($("#BVN").val() == "" && $("#NIN").val() == "" ){
    swal("Notification", "Please fill either your BVN or NIN number", "error");
    return;
  }
  else if (
    $("#TitleId").val() == "" ||
    $("#Sex").val() == "" ||
    $("#LastName").val() == "" ||
    $("#check_error").val() == "" ||
    $("#DateOfBirth").val() == "" ||
    $("#Nationality").val() == "" ||
    $("#StateOfOrigin").val() == "" ||
    $("#LGAOfOrigin").val() == "" ||
    $("#physically_challenged").val() == ""
  ) {
    swal("Notification", "Please fill all required fields", "error");
    return;
  }
  else {
    var personalData = {};
    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      // for (let i = 0; i < salary_structureArray.length; i++) {
      //   publicSectorObject = {
      //     RefId: Math.random()
      //       .toString()
      //       .substr(2, 10),
      //     SalaryStructure: salary_structureArray[i],
      //     GL: grade_levelArray[i],
      //     Step: grade_stepArray[i],
      //     CaptureType: 1,
      //     Salary: salaryArray[i]
      //   };
      //   publicSectorArray.push(publicSectorObject);
      // }

      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val() ? $("#BVN").val() : '00000000000',
      NIN: $("#NIN").val() ? $("#NIN").val() : '00000000000',
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("fieldset").css({ display: "none" });
        $("#final_div").removeClass("hidden");
      }
    });
  }
}

// Save Cntact Address
function saveContactData() {
  let validateEmail = isEmail($("#Email").val());
  if (validateEmail == false) {
    $("#Email").val("");
    swal("Notification", "Invalid email", "error");
    return;
  }

  if ($("#TelephoneNo").val() == "") {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};

    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val(),
      NIN: $("#NIN").val(),
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("#review_div").html("");
        // reviewData();

        if (response.StatusCode == "00") {
          swal("Good job!", "success");
        }
      }
    });
  }
}

function saveContactDataLater() {
  let validateEmail = isEmail($("#Email").val());
  if (validateEmail == false) {
    $("#Email").val("");
    swal("Notification", "Invalid email", "error");
    return;
  }
  if ($("#TelephoneNo").val() == "") {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};

    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val(),
      NIN: $("#NIN").val(),
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("fieldset").css({ display: "none" });
        $("#final_div").removeClass("hidden");
      }
    });
  }
}

function saveEmploymentData() {
  if ($("#SectorID").val() == "") {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};

    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val(),
      NIN: $("#NIN").val(),
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("#review_div").html("");
        // reviewData();

        if (response.StatusCode == "00") {
          swal("Good job!", "success");
        }
      }
    });
  }
}

function saveEmploymentDataLater() {
  if ($("#SectorID").val() == "") {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};

    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val(),
      NIN: $("#NIN").val(),
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("fieldset").css({ display: "none" });
        $("#final_div").removeClass("hidden");
      }
    });
  }
}

function saveNokData() {
  if (
    $("#NOKLastName").val() == "" ||
    $("#NOKFirstName").val() == "" ||
    // $("#NOKOtherNames").val() == "" ||
    $("#NOKRelationship").val() == "" ||
    $("#NOKCountryCode").val() == "" ||
    $("#NOKSex").val() == "" ||
    $("#NOKMobile").val() == ""
  ) {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};

    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val(),
      NIN: $("#NIN").val(),
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("#review_div").html("");
        reviewData();

        if (response.StatusCode == "00") {
          swal("Good job!", "success");
        }
      }
    });
  }
}

function saveNokDataLater() {
  if (
    $("#NOKLastName").val() == "" ||
    $("#NOKFirstName").val() == "" ||
    // $("#NOKOtherNames").val() == "" ||
    $("#NOKRelationship").val() == "" ||
    $("#NOKCountryCode").val() == "" ||
    $("#nokmobileno").val() == ""
  ) {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};

    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val(),
      NIN: $("#NIN").val(),
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/SaveTempCustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        $("fieldset").css({ display: "none" });
        $("#final_div").removeClass("hidden");
      }
    });
  }
}

function saveCertification() {
  if ($("#profile_pic_ppp").val() == "" || $("#txtSignature").val() == "") {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    return true;
  }
}

function finalSubmit() {
  if ($("#profile_pic_ppp").val() == "" || $("#txtSignature").val() == "") {
    swal("Notification", "Please fill all required fields", "error");
    return;
  } else {
    var personalData = {};

    // Loop through Federal Sector if it exist
    publicSectorArray = [];
    publicSectorObject = {};

    salary_structureArray = [];
    grade_levelArray = [];
    grade_stepArray = [];
    salaryArray = [];

    sectorID = $("#SectorID").val();
    if (sectorID == "1" || sectorID == "4" || sectorID == "5") {
      $(".salary_structure").each(function() {
        publicSectorObject = {
          SalaryStructure: $(this).val()
        };
        salary_structureArray.push($(this).val());
      });
      console.log(publicSectorObject);

      $(".grade_level").each(function() {
        grade_levelArray.push($(this).val());
      });

      $(".grade_step").each(function() {
        grade_stepArray.push($(this).val());
      });

      $(".salary").each(function() {
        salaryArray.push($(this).val());
      });

      // Loop through all of them
      for (let i = 0; i < salary_structureArray.length; i++) {
        let year = salary_structureArray[i].split("_");
        publicSectorObject = {
          RefId: Math.random()
            .toString()
            .substr(2, 8),
          SalaryStructure: salary_structureArray[i],
          GL: grade_levelArray[i],
          Step: grade_stepArray[i],
          CaptureType: 1,
          Salary: salaryArray[i],
          Year: year[1]
        };
        publicSectorArray.push(publicSectorObject);
      }
    }

    personalData = {
      RSAPIN: $("#RSAPIN").val(),
      AgentCode: $("#AgentCode").val(),
      LPPFAUsername: $("#LPPFAUsername").val(),
      UserId: $("#UserId").val(),
      TitleId: $("#TitleId").val(),
      LastName: $("#LastName").val(),
      FirstName: $("#FirstName").val(),
      OtherName: $("#OtherName").val(),
      Sex: $("#Sex").val(),
      MaritalStatus: $("#MaritalStatus").val(),
      MaidenName: $("#MaidenName").val(),
      PlaceOfBirth: $("#PlaceOfBirth").val(),
      DateOfBirth: $("#DateOfBirth").val(),
      Nationality: $("#Nationality").val(),
      StateOfOrigin: $("#StateOfOrigin").val(),
      LGAOfOrigin: $("#LGAOfOrigin").val(),
      OtherPFA: $("#OtherPFA").val(),
      physically_challenged: $("#physically_challenged").val(),
      OtherPencomPIN: $("#OtherPencomPIN").val(),
      BVN: $("#BVN").val(),
      NIN: $("#NIN").val(),
      id_card_type: $("#id_card_type").val(),
      id_cardno: $("#id_cardno").val(),
      HouseNumberName: $("#HouseNumberName").val(),
      StreetName: $("#StreetName").val(),
      VillageTownCity: $("#VillageTownCity").val(),
      CityCode: $("#CityCode").val(),
      StateCode: $("#StateCode").val(),
      lga_res: $("#lga_res").val(),
      TelephoneNo: $("#TelephoneNo").val(),
      phoneno: $("#phoneno").val(),
      Email: $("#Email").val(),
      SectorID: $("#SectorID").val(),
      DateOfAppointment: $("#DateOfAppointment").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      ServiceId: $("#ServiceId").val(),
      ServiceNo: $("#ServiceNo").val(),
      IPPISJoinDate: $("#IPPISJoinDate").val(),
      customer_ippis: $("#customer_ippis").val(),
      DateOfCurrentEmployment: $("#DateOfCurrentEmployment").val(),
      CurrentAppointmentDate: $("#CurrentAppointmentDate").val(),
      TransferOfServiceDate: $("#TransferOfServiceDate").val(),
      EmployeeNo: $("#EmployeeNo").val(),
      Industry: $("#Industry").val(),
      Qualification: $("#Qualification").val(),
      EmployerBuildingNumberName: $("#EmployerBuildingNumberName").val(),
      StreetName: $("#StreetName").val(),
      EmployerVillageTownCity: $("#EmployerVillageTownCity").val(),
      ExpectedEEMonthlyContr: $("#ExpectedEEMonthlyContr").val(),
      ExpectedERMonthlyContr: $("#ExpectedERMonthlyContr").val(),
      VoluntryContribution: $("#VoluntryContribution").val(),
      Designation: $("#Designation").val(),
      NOKTitle: $("#NOKTitle").val(),
      NOKRelationship: $("#NOKRelationship").val(),
      NOKFirstName: $("#NOKFirstName").val(),
      NOKOtherNames: $("#NOKOtherNames").val(),
      NOKLastName: $("#NOKLastName").val(),
      NOKHouseNumberName: $("#NOKHouseNumberName").val(),
      NOKStreetName: $("#NOKStreetName").val(),
      NOKVillageTownCity: $("#NOKVillageTownCity").val(),
      NOKCityCode: $("#NOKCityCode").val(),
      NOKStateCode: $("#NOKStateCode").val(),
      NOKCountryCode: $("#NOKCountryCode").val(),
      NOKEmail: $("#NOKEmail").val(),
      NOKPhone: $("#NOKPhone").val(),
      NOKMobile: $("#NOKMobile").val(),
      NOKSex: $("#NOKSex").val(),
      OtherPFA: $("#OtherPFA").val(),
      IPPISNO: $("#IPPISNO").val(),
      PublicSectorEmploymentSalaryData: publicSectorArray,
      CFIBiometrics: [
        {
          BiometricType: 11,
          Value: $("#profile_pic_ppp").val()
        },
        {
          BiometricType: 12,
          Value: $("#txtSignature").val()
        },
        {
          BiometricType: 13,
          Value: $("#txtidCard").val()
        },
        {
          BiometricType: 14,
          Value: $("#txtBirthCertificate").val()
        },
        {
          BiometricType: 15,
          Value: $("#txtMeansOfID").val()
        },
        {
          BiometricType: 16,
          Value: $("#txtProofOfResidency").val()
        },
        {
          BiometricType: 17,
          Value: $("#txtPhyChallengeDocument").val()
        },
        {
          BiometricType: 18,
          Value: $("#txtDoc4").val()
        }
      ]
    };

    // personalData = JSON.stringify(personalData);
    console.log(personalData);
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/CustomerDataUpdate",
      data: personalData,
      cache: false,
      beforeSend: function() {
        $.blockUI({
          css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 0.5,
            color: "#fff"
          }
        });
      },
      success: function(response) {
        $.unblockUI();
        console.log(response);
        if (response.StatusCode == "00") {
          swal(response.Message, "success");
        }
      }
    });
  }
}

function reviewData() {
  // Load data to fieldset
  $("#title option:selected").html(
    "<strong>Title : </strong>" + $("#TitleId option:selected").text()
  );
  $("#lastname").html("<strong>Surname : </strong>" + $("#LastName").val());
  $("#firstname").html("<strong>Firstname : </strong>" + $("#FirstName").val());
  $("#othername").html("<strong>Othername : </strong>" + $("#othername").val());
  $("#sex").html("<strong>Sex : </strong>" + $("#Sex option:selected").text());
  $("#marital_status").html(
    "<strong>Marital Status : </strong>" +
      $("#MaritalStatus option:selected").text()
  );
  $("#maiden_name").html(
    "<strong>Maiden Name : </strong>" + $("#MaidenName").val()
  );
  $("#place_birth").html(
    "<strong>Place of Birth : </strong>" + $("#PlaceOfBirth").val()
  );
  $("#date_birth").html(
    "<strong>Date of Birth : </strong>" + $("#DateOfBirth").val()
  );
  $("#country").html(
    "<strong>Country of orirgin : </strong>" +
      $("#Nationality option:selected").text()
  );
  $("#state_origin").html(
    "<strong>State of Origin : </strong>" +
      $("#StateOfOrigin option:selected").text()
  );
  $("#lga_origin").html(
    "<strong>Local Goverment : </strong>" +
      $("#LGAOfOrigin option:selected").text()
  );
  $("#other_pfa").html(
    "<strong>OtherPFA : </strong>" + $("#OtherPFA option:selected").text()
  );
  $("#other_pencom_pin").html(
    "<strong>Other Pencom Pin : </strong>" + $("#OtherPencomPIN").val()
  );
  $("#physically_challenged_view").html(
    "<strong>Physically Challenged : </strong>" +
      $("#physically_challenged option:selected").text()
  );
  $("#rsa_pin").html("<strong>PIN ID : </strong>" + $("#LastName").val());
  $("#bvn").html("<strong>BVN : </strong>" + $("#BVN").val());
  $("#nin").html("<strong>NIN : </strong>" + $("#NIN").val());
  $("#house_number_name").html(
    "<strong>House Number : </strong>" + $("#HouseNumberName").val()
  );
  $("#street_name").html(
    "<strong>Street Name : </strong>" + $("#StreetName").val()
  );
  $("#village_town_city").html(
    "<strong>Town / City : </strong>" + $("#VillageTownCity").val()
  );

  $("#country_residenece").html(
    "<strong>Resident Country : </strong>" +
      $("#CityCode option:selected").text()
  );
  $("#state_residence").html(
    "<strong>Resident State : </strong>" + $("#StateCode").val()
  );
  $("#lga_residence").html(
    "<strong>Resident LGA : </strong>" + $("#lga_res").val()
  );
  $("#mobile_number").html(
    "<strong>Mobile Number : </strong>" + $("#TelephoneNo").val()
  );
  $("#phone_number").html(
    "<strong>Phone Number : </strong>" + $("#phoneno").val()
  );
  $("#email_address").html(
    "<strong>Email Address : </strong>" + $("#Email").val()
  );

  $("#sector_id").html(
    "<strong>Sector : </strong>" + $("#SectorID option:selected").text()
  );
  if (
    $("#SectorID").val() === "1" ||
    $("#SectorID").val() === "4" ||
    $("#SectorID").val() === "5"
  ) {
    $("#service_no").html(
      "<strong>Service No : </strong>" + $("#ServiceNo").val()
    );
    $("#current_appointment_date").html(
      "<strong>Current Appointment No : </strong>" +
        $("#CurrentAppointmentDate").val()
    );

    $("#customer_ippis_cs").html(
      "<strong>Customer IPPIS : </strong>" +
        $("#customer_ippis option:selected").text()
    );
    $("#ippis_no").html("<strong>IPPIS No : </strong>" + $("#IPPISNO").val());
    $("#ippis_date").html(
      "<strong>IPPIS Join Date : </strong>" + $("#TransferOfServiceDate").val()
    );

    $("#current_appointment_date").html("");
    $("#OfficeTelNo").html("");
    $("#OfficialEmailAddress").html("");
    $("#emp_email").html("");

    // Fetch public salary structure
    $.ajax({
      type: "POST",
      headers: { AppKey: "54321" },
      url:
        "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/GetTempCustomerDataUpdate?rsapin=" +
        pin,
      success: function(response) {
        $("#public_sector").html("");

        let PublicSectorEmploymentSalaryData =
          response.Data.PublicSectorEmploymentSalaryData;
        PublicSectorEmploymentSalaryData.forEach(function(e) {
          $("#public_sector").append(
            "<div><strong>Salary Structure:</strong> " +
              e.SalaryStructure +
              "</div>" +
              "<div><strong>Year:</strong> " +
              e.Year +
              "</div>" +
              "<div><strong>Grade Level: </strong> " +
              e.GL +
              "</div>" +
              "<div><strong>Grade Step: </strong> " +
              e.Step +
              "</div>" +
              "<div><strong>Salary: </strong> " +
              e.Salary +
              "</div>"
          );
        });
      }
    });
  } else {
    $("#current_appointment_date").html(
      "<strong>Current Appointment No : </strong>" +
        $("#DateOfCurrentEmployment").val()
    );
    $("#emp_phone").html(
      "<strong>Employer Phone Number : </strong>" + $("#OfficeTelNo").val()
    );
    $("#emp_email").html(
      "<strong>Employer Phone Number : </strong>" +
        $("#OfficialEmailAddress").val()
    );

    $("#current_appointment_date").html("");
    $("#service_no").html("");
    $("#ippis_no").html("");
    $("#public_sector").html("");
  }

  $("#employer_code").html(
    "<strong>Employer Code : </strong>" + $("#EmployerCode").val()
  );
  $("#employer_id").html(
    "<strong>Employer Id : </strong>" + $("#EmployerId").val()
  );
  $("#organisation_name").html(
    "<strong>Organisation : </strong>" + $("#org_name").val()
  );
  $("#date_retirement").html(
    "<strong>Retirement Date : </strong>" + $("#retirement_date").val()
  );
  $("#date_first_emp").html(
    "<strong>Date of First Employement : </strong>" + $("#IPPISJoinDate").val()
  );
  $("#date_appointment").html(
    "<strong>Date of Appointment : </strong>" + $("#DateOfAppointment").val()
  );
  $("#date_curent_emp").html(
    "<strong>Current Employement Date : </strong>" +
      $("#DateOfCurrentEmployment").val()
  );
  $("#monthly_total_emolument").html(
    "<strong><Monthly Total Emolument </strong>" +
      $("#MonthlyTotalEmolument").val()
  );
  $("#expected_ee_monthly_contr").html(
    "<strong>Expected EE Monthly Contribution : </strong>" +
      $("#ExpectedEEMonthlyContr").val()
  );
  $("#expected_er_monthly_contr").html(
    "<strong>Expected ER Monthly Contribution : </strong>" +
      $("#ExpectedERMonthlyContr").val()
  );
  $("#voluntry_contribution").html(
    "<strong>Voluntry Contribution : </strong>" +
      $("#VoluntryContribution").val()
  );

  $("#qualification_emp").html(
    "<strong>Qualification : </strong>" +
      $("#Qualification option:selected").text()
  );
  $("#occupation_emp").html(
    "<strong>Occupation : </strong>" + $("#Occupation option:selected").text()
  );
  $("#emp_streetname").html(
    "<strong>Street Name : </strong>" + $("#StreetName").val()
  );
  $("#emp_building_number_name").html(
    "<strong>Employer Building Number / Name : </strong>" +
      $("#EmployerBuildingNumberName").val()
  );
  $("#emp_village").html(
    "<strong>Village / Town / City : </strong>" +
      $("#EmployerVillageTownCity").val()
  );
  $("#emp_pobox").html("<strong>P.O. Box : </strong>" + $("#pobox").val());

  $("#emp_designation").html(
    "<strong>Designation : </strong>" + $("#Designation").val()
  );
  $("#industry_name").html(
    "<strong>Industry : </strong>" + $("#Industry option:selected").text()
  );
  $("#emp_mobileno").html(
    "<strong> Phone Number : </strong>" + $("#mobileno").val()
  );

  $("#nok_title").html(
    "<strong>Title : </strong>" + $("#NOKTitle option:selected").text()
  );
  $("#nok_lastname").html(
    "<strong>Lastname : </strong>" + $("#NOKLastName").val()
  );
  $("#nok_firstname").html(
    "<strong>Firstname : </strong>" + $("#NOKFirstName").val()
  );
  $("#nok_othername").html(
    "<strong>Othername : </strong>" + $("#VillageTownCity").val()
  );
  $("#nok_sex").html(
    "<strong>Sex : </strong>" + $("#NOKSex option:selected").text()
  );
  $("#nok_relationship").html(
    "<strong>Relationship : </strong>" +
      $("#NOKRelationship option:selected").text()
  );
  $("#nok_mobile").html("<strong>Mobile : </strong>" + $("#NOKMobile").val());
  $("#nok_phone").html("<strong>Phone : </strong>" + $("#NOKPhone").val());
  $("#nok_email").html("<strong>Email : </strong>" + $("#NOKEmail").val());
  $("#nok_country").html(
    "<strong>Country : </strong>" + $("#NOKCountryCode option:selected").text()
  );
  $("#nok_state").html(
    "<strong>State : </strong>" + $("#NOKStateCode option:selected").text()
  );
  $("#nok_lga").html(
    "<strong>Local Goverment : </strong>" +
      $("#NOKCityCode option:selected").text()
  );
  $("#nok_village").html(
    "<strong>Village : </strong>" + $("#NOKVillageTownCity").val()
  );
  $("#nok_house_number").html(
    "<strong>House Number : </strong>" + $("#NOKHouseNumberName").val()
  );
  $("#nok_pobox").html("<strong>P.O. Box : </strong>" + $("#pobox").val());
  $("#nok_streetname").html(
    "<strong>Streetname : </strong>" + $("#NOKStreetName").val()
  );
}

function PrintElem(elem, e) {
  // var mywindow = window.open("", "PRINT", "height=400,width=600");

  // mywindow.document.write("<html><head><title>" + document.title + "</title>");
  // mywindow.document.write("</head><body >");
  // mywindow.document.write("<h1>" + document.title + "</h1>");
  // mywindow.document.write(document.getElementById(elem).innerHTML);
  // mywindow.document.write("</body></html>");

  // mywindow.document.close(); // necessary for IE >= 10
  // mywindow.focus(); // necessary for IE >= 10*/

  // mywindow.print();
  // mywindow.close();
  window.print();
  e.preventDefault();
  // return true;
}

function openPrintDialogue(e) {
  e.preventDefault();
  $("<iframe>", {
    name: "myiframe",
    class: "printFrame"
  })
    .attr("src", "print_review.php")
    .appendTo("body")
    .contents()
    .find("body");

  window.frames["myiframe"].focus();
  window.frames["myiframe"].print();

  setTimeout(() => {
    $(".printFrame").remove();
  }, 1000);
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
