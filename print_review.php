<?php 
session_start(); 
require 'config/Leadway.php';

// function findSchedules($workHours, $dayHours, $pattern) {
    // Write your code here
    $workHours = 20; $dayHours=5; $pattern="???5???";
    $output = [];
    $diff = $workHours - $dayHours;
    $n = 8;
    $sum = $dayHours;

    // $split_pattern = explode(",", $pattern);
    $split_pattern = str_split($pattern);
    foreach($split_pattern as $pp){
      if($pp == "?"){
        if ($sum < $workHours) {
          if ($diff > $n) {
            $output[] = $n;
            $diff = $diff - $n;
            $sum = $sum + $n;
          } else {
            $output[] = $diff;
            $sum = $sum + $diff;
          }
        } else {
          $output[] = 0;
        }
      }
      else{
        $output[] = $pp;
      }
    }
    // return $output;
    echo implode(",", $output);
// }

// $val = findSchedules(56,8,'???8???');
// echo "<pre>";
// print_r($val);
// echo "</pre>";

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="css_cfi/bootstrap.min.css" rel="stylesheet"/>
  <script src="js_cfi/jquery.min.js"></script>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <?php echo $_SESSION['pin'] ?>
        <h5>Bio Data</h5>
        <p id="title"></p>
        <p id="lastname"></p>
        <p id="firstname"></p>
        <p id="othername"></p>
        <p id="sex"></p>
        <p id="marital_status"></p>
        <p id="maiden_name"></p>
        <p id="place_birth"></p>
        <p id="date_birth"></p>
        <p id="country"></p>
        <p id="state_origin"></p>
        <p id="lga_origin"></p>
        <p id="other_pfa"></p>
        <p id="other_pencom_pin"></p>
        <p id="physically_challenged_view"></p>
        <p id="rsa_pin"></p>
        <p id="bvn"></p>
        <p id="nin"></p>
      </div>
      <div class="col-sm-6">
        <h5>Contact Address</h5>
        <p id="house_number_name"></p>
        <p id="street_name"></p>
        <p id="village_town_city"></p>
        <p id="country_residenece"></p>
        <p id="state_residence"></p>
        <p id="lga_residenece"></p>
        <p id="mobile_number"></p>
        <p id="phone_number"></p>
        <p id="email_address"></p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <h5>Employment Details</h5>
        <p id="sector_id"></p>
        <p id="public_sector"></p>
        <p id="service_no"></p>
        <p id="current_appointment_date"></p>
        <p id="ippis_no"></p>
        <p id="customer_ippis_cs"></p>
        <p id="ippis_date"></p>
        <p id="organisation_name"></p>
        <p id="industry_name"></p>
        <p id="employer_id"></p>
        <p id="employer_code"></p>
        <p id="date_retirement"></p>
        <p id="date_curent_emp"></p>
        <p id="date_first_emp"></p>
        <p id="monthly_total_emolument"></p>
        <p id="expected_ee_monthly_contr"></p>
        <p id="expected_er_monthly_contr"></p>
        <p id="voluntry_contribution"></p>
        <p id="qualification_emp"></p>
        <p id="occupation_emp"></p>
        <p id="emp_building_number_name"></p>
        <p id="emp_streetname"></p>
        <p id="emp_village"></p>
        <p id="emp_pobox"></p>
        <p id="emp_mobileno"></p>
        <p id="emp_email"></p>
        <p id="emp_designation"></p>
        <p id="date_of_current_emp"></p>
        <p id="office_tel_no"></p>
        <p id="official_email_address"></p>
      </div>
      <div class="col-sm-6">
        <h5>Next of Kin</h5>
        <p id="nok_title"></p>
        <p id="nok_lastname"></p>
        <p id="nok_firstname"></p>
        <p id="nok_othername"></p>
        <p id="nok_sex"></p>
        <p id="nok_relationship"></p>
        <p id="nok_mobile"></p>
        <p id="nok_phone"></p>
        <p id="nok_email"></p>
        <p id="nok_country"></p>
        <p id="nok_state"></p>
        <p id="nok_lga"></p>
        <p id="nok_village"></p>
        <p id="nok_house_number"></p>
        <p id="nok_pobox"></p>
        <p id="nok_streetname"></p>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      const pin = '<?php echo $_SESSION['pin'] ?>';
  </script>
  <script>
    $(document).ready(function(){
      $.ajax({
        type: "POST",
        headers: { AppKey: "54321" },
        url:
          "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/GetTempCustomerDataUpdate?rsapin=" +
          pin,
        success: function(result) {
          if(response.Data.TitleId == '1'){
            $("#title").html("<strong>Title : </strong> Male");
          }
          elseif(response.Data.TitleId == '2'){
            $("#title").html("<strong>Title : </strong> Mrs");
          }
          elseif(response.Data.TitleId == '3'){
            $("#title").html("<strong>Title : </strong> Miss");
          }
          else{
            $("#title").html("<strong>Title : </strong> MS");
          }
            
        $("#lastname").html("<strong>Surname : </strong>" + response.Data.LastName);
        $("#firstname").html("<strong>Firstname : </strong>" + response.Data.FirstName);
        $("#othername").html("<strong>Othername : </strong>" + response.Data.OtherName);
        $("#sex").html("<strong>Sex : </strong>" + $("#Sex option:selected").text());
        $("#marital_status").html(
          "<strong>Marital Status : </strong>" +
            $("#MaritalStatus option:selected").text()
        );
        $("#maiden_name").html(
          "<strong>Maiden Name : </strong>" + response.Data.MaidenName
        );
        $("#place_birth").html(
          "<strong>Place of Birth : </strong>" + response.Data.PlaceOfBirth
        );
        $("#date_birth").html(
          "<strong>Date of Birth : </strong>" + response.Data.DateOfBirth
        );
        $("#country").html(
          "<strong>Country of orirgin : </strong>" +
            $("#Nationality option:selected").text()
        );
        $("#state_origin").html(
          "<strong>State of Origin : </strong>" +
            $("#StateOfOrigin option:selected").text()
        );
        $("#lga_origin").html(
          "<strong>Local Goverment : </strong>" +
            $("#LGAOfOrigin option:selected").text()
        );
        $("#other_pfa").html(
          "<strong>OtherPFA : </strong>" + $("#OtherPFA option:selected").text()
        );
        $("#other_pencom_pin").html(
          "<strong>Other Pencom Pin : </strong>" + $("#OtherPencomPIN").val()
        );
        $("#physically_challenged_view").html(
          "<strong>Physically Challenged : </strong>" +
            $("#physically_challenged option:selected").text()
        );
        $("#rsa_pin").html("<strong>PIN ID : </strong>" + $("#LastName").val());
        $("#bvn").html("<strong>BVN : </strong>" + $("#BVN").val());
        $("#nin").html("<strong>NIN : </strong>" + $("#NIN").val());
        $("#house_number_name").html(
          "<strong>House Number : </strong>" + $("#HouseNumberName").val()
        );
        $("#street_name").html(
          "<strong>Street Name : </strong>" + $("#StreetName").val()
        );
        $("#village_town_city").html(
          "<strong>Town / City : </strong>" + $("#VillageTownCity").val()
        );

        $("#country_residenece").html(
          "<strong>Resident Country : </strong>" +
            $("#CityCode option:selected").text()
        );
        $("#state_residence").html(
          "<strong>Resident State : </strong>" + $("#state_res").val()
        );
        $("#lga_residence").html(
          "<strong>Resident LGA : </strong>" + $("#lga_res").val()
        );
        $("#mobile_number").html(
          "<strong>Mobile Number : </strong>" + $("#TelephoneNo").val()
        );
        $("#phone_number").html(
          "<strong>Phone Number : </strong>" + $("#phoneno").val()
        );
        $("#email_address").html(
          "<strong>Email Address : </strong>" + $("#Email").val()
        );

        $("#sector_id").html(
          "<strong>Sector : </strong>" + $("#SectorID option:selected").text()
        );
        if (
          $("#SectorID").val() === "1" ||
          $("#SectorID").val() === "4" ||
          $("#SectorID").val() === "5"
        ) {
          $("#service_no").html(
            "<strong>Service No : </strong>" + $("#ServiceNo").val()
          );
          $("#current_appointment_date").html(
            "<strong>Current Appointment No : </strong>" +
              $("#CurrentAppointmentDate").val()
          );

          $("#customer_ippis_cs").html(
            "<strong>Customer IPPIS : </strong>" +
              $("#customer_ippis option:selected").text()
          );
          $("#ippis_no").html("<strong>IPPIS No : </strong>" + $("#IPPISNO").val());
          $("#ippis_date").html(
            "<strong>IPPIS Join Date : </strong>" + $("#TransferOfServiceDate").val()
          );

          $("#current_appointment_date").html("");
          $("#OfficeTelNo").html("");
          $("#OfficialEmailAddress").html("");
          $("#emp_email").html("");

          // Fetch public salary structure
          $.ajax({
            type: "POST",
            headers: { AppKey: "54321" },
            url:
              "https://mapps.leadway-pensure.com/MobileEnrolmentUAT/EEnrolment/GetTempCustomerDataUpdate?rsapin=" +
              pin,
            success: function(response) {
              $("#public_sector").html("");

              let PublicSectorEmploymentSalaryData =
                response.Data.PublicSectorEmploymentSalaryData;
              PublicSectorEmploymentSalaryData.forEach(function(e) {
                $("#public_sector").append(
                  "<div><strong>Salary Structure:</strong> " +
                    e.SalaryStructure +
                    "</div>" +
                    "<div><strong>Year:</strong> " +
                    e.Year +
                    "</div>" +
                    "<div><strong>Grade Level: </strong> " +
                    e.GL +
                    "</div>" +
                    "<div><strong>Grade Step: </strong> " +
                    e.Step +
                    "</div>" +
                    "<div><strong>Salary: </strong> " +
                    e.Salary +
                    "</div>"
                );
              });
            }
          });
        } else {
          $("#current_appointment_date").html(
            "<strong>Current Appointment No : </strong>" +
              $("#DateOfCurrentEmployment").val()
          );
          $("#emp_phone").html(
            "<strong>Employer Phone Number : </strong>" + $("#OfficeTelNo").val()
          );
          $("#emp_email").html(
            "<strong>Employer Phone Number : </strong>" +
              $("#OfficialEmailAddress").val()
          );

          $("#current_appointment_date").html("");
          $("#service_no").html("");
          $("#ippis_no").html("");
          $("#public_sector").html("");
        }

        $("#employer_code").html(
          "<strong>Employer Code : </strong>" + $("#EmployerCode").val()
        );
        $("#employer_id").html(
          "<strong>Employer Id : </strong>" + $("#EmployerId").val()
        );
        $("#organisation_name").html(
          "<strong>Organisation : </strong>" + $("#org_name").val()
        );
        $("#date_retirement").html(
          "<strong>Retirement Date : </strong>" + $("#retirement_date").val()
        );
        $("#date_curent_emp").html(
          "<strong>Current Employement Date : </strong>" + $("#DateOfCurrentEmployment").val()
        );
        $("#monthly_total_emolument").html(
          "<strong><Monthly Total Emolument </strong>" +
            $("#MonthlyTotalEmolument").val()
        );
        $("#expected_ee_monthly_contr").html(
          "<strong>Expected EE Monthly Contribution : </strong>" +
            $("#ExpectedEEMonthlyContr").val()
        );
        $("#expected_er_monthly_contr").html(
          "<strong>Expected ER Monthly Contribution : </strong>" +
            $("#ExpectedERMonthlyContr").val()
        );
        $("#voluntry_contribution").html(
          "<strong>Voluntry Contribution : </strong>" +
            $("#VoluntryContribution").val()
        );

        $("#qualification_emp").html(
          "<strong>Qualification : </strong>" +
            $("#Qualification option:selected").text()
        );
        $("#occupation_emp").html(
          "<strong>Occupation : </strong>" + $("#Occupation option:selected").text()
        );
        $("#emp_streetname").html(
          "<strong>Street Name : </strong>" + $("#StreetName").val()
        );
        $("#emp_building_number_name").html(
          "<strong>Employer Building Number / Name : </strong>" +
            $("#EmployerBuildingNumberName").val()
        );
        $("#emp_village").html(
          "<strong>Village / Town / City : </strong>" +
            $("#EmployerVillageTownCity").val()
        );
        $("#emp_pobox").html("<strong>P.O. Box : </strong>" + $("#pobox").val());

        $("#emp_designation").html(
          "<strong>Designation : </strong>" + $("#Designation").val()
        );
        $("#industry_name").html(
          "<strong>Industry : </strong>" + $("#Industry option:selected").text()
        );
        $("#emp_mobileno").html(
          "<strong> Phone Number : </strong>" + $("#mobileno").val()
        );

        $("#nok_title").html(
          "<strong>Title : </strong>" + $("#NOKTitle option:selected").text()
        );
        $("#nok_lastname").html(
          "<strong>Lastname : </strong>" + $("#NOKLastName").val()
        );
        $("#nok_firstname").html(
          "<strong>Firstname : </strong>" + $("#NOKFirstName").val()
        );
        $("#nok_othername").html(
          "<strong>Othername : </strong>" + $("#VillageTownCity").val()
        );
        $("#nok_sex").html(
          "<strong>Sex : </strong>" + $("#NOKSex option:selected").text()
        );
        $("#nok_relationship").html(
          "<strong>Relationship : </strong>" +
            $("#NOKRelationship option:selected").text()
        );
        $("#nok_mobile").html("<strong>Mobile : </strong>" + $("#NOKMobile").val());
        $("#nok_phone").html("<strong>Phone : </strong>" + $("#NOKPhone").val());
        $("#nok_email").html("<strong>Email : </strong>" + $("#NOKEmail").val());
        $("#nok_country").html(
          "<strong>Country : </strong>" + $("#NOKCountryCode option:selected").text()
        );
        $("#nok_state").html(
          "<strong>State : </strong>" + $("#NOKStateCode option:selected").text()
        );
        $("#nok_lga").html(
          "<strong>Local Goverment : </strong>" +
            $("#NOKCityCode option:selected").text()
        );
        $("#nok_village").html(
          "<strong>Village : </strong>" + $("#NOKVillageTownCity").val()
        );
        $("#nok_house_number").html(
          "<strong>House Number : </strong>" + $("#NOKHouseNumberName").val()
        );
        $("#nok_pobox").html("<strong>P.O. Box : </strong>" + $("#pobox").val());
        $("#nok_streetname").html(
          "<strong>Streetname : </strong>" + $("#NOKStreetName").val()
        );
        }  
    });
  </script>
</body>
</html>