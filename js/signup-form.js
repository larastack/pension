document.addEventListener("touchstart",function(){},false);
(function($) { "use strict";
    $(function(){ 
        var randNumber_1=parseInt(Math.ceil(Math.random()*15),10);
        var randNumber_2=parseInt(Math.ceil(Math.random()*15),10);
        humanCheckCaptcha(randNumber_1,randNumber_2);
    });
    function humanCheckCaptcha(randNumber_1,randNumber_2){
        $("#humanCheckCaptchaBox").html("Solve The Math ");
        $("#firstDigit").html('<input name="mathfirstnum" id="mathfirstnum" class="form-control" type="text" value="'+randNumber_1+'" readonly>');
        $("#secondDigit").html('<input name="mathsecondnum" id="mathsecondnum" class="form-control" type="text" value="'+randNumber_2+'" readonly>');
    }
    $(function(){
        $('#date-of-retirement input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        $('#date-of-first-employment-public input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        $('#date-of-current-employment-public input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        $('#date-of-transfer-of-service input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        $('#date-of-employment input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        $('#date-of-joined-ippis input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        $('#date-of-birth input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        $('#date-of-birth-new input').datepicker({format:"yyyy-mm-dd",todayBtn:"linked",todayHighlight:true,autoclose:true});
        
    });
    $("#signUpForm").validator().on("submit",function(event) {
        if(event.isDefaultPrevented()){
            formError();submitMSG(false,"Kindly ensure all mandatory fields are filled!");
        } else {
            var mathPart_1=parseInt($("#mathfirstnum").val(),10);
            var mathPart_2=parseInt($("#mathsecondnum").val(),10);
            var correctMathSolution=parseInt((mathPart_1+mathPart_2),10);
            var inputHumanAns=$("#humanCheckCaptchaInput").val();
            if(inputHumanAns==correctMathSolution){
                event.preventDefault();submitForm();
            } else {
                submitMSG(false,"Please solve Human Captcha!!!");return false;
            }
        }
    });
    function submitForm(){
        $("#mgsFormSubmit").html('');
        $("#final-step-buttons").html('<div class="h3 text-center text-success"> You have finished all steps of this html form successfully </div>');
    }
    // $(function(){
    //     $(document).on('change',':file',function(){
    //         var input=$(this),numFiles=input.get(0).files?input.get(0).files.length:1,label=input.val().replace(/\\/g,'/').replace(/.*\//,'');input.trigger('fileselect',[numFiles,label]);
    //     });
    //     $(':file').on('fileselect',function(event,numFiles,label){
    //         var input=$(this).parents('.form-group').find(':text'),log=numFiles>1?numFiles+' files selected':label;
    //         if(input.length){
    //             input.val(log);
    //         }else{
    //             if(log)alert(log);
    //         }
    //     });
    // });
    // function readURL(input){
    //     if(input.files&&input.files[0]){
    //         var reader=new FileReader();
    //         reader.onload=function(e){
    //             $('#userPhoto').attr('src',e.target.result);
    //         }
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // } 
    // $("#userfile").on('change',function(){readURL(this);});function formSuccess(){$("#signUpForm")[0].reset();submitMSG(true,"Registration Process Successfully!")}

    function formError(){ 
        $(".help-block.with-errors").removeClass('hidden');
    }

    function submitMSG(valid,msg) { 
        if(valid) { 
            var msgClasses="h3 text-center text-success";
        } else { 
            var msgClasses="h3 text-center text-danger";
        } 
        $("#mgsFormSubmit").removeClass().addClass(msgClasses).text(msg);
    }
})(jQuery);

$(function(){$("#signUpForm").on('focus',':input',function(){$(this).attr('autocomplete','off');});});

function isEmail(email) { 
    var regex=/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;return regex.test(email);
}

function uploadSignature(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtSignatureCert').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadNoSignature(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtNoSignatureCert').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadDOB(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtDOBCert').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPassport(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPassportImage').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPOI(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPOI').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPOA(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPOA').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadEvidenceEmployment(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtEvidenceOfEmploymentImage').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadTransferImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtEmpTransferImage').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPaySlip2004(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPaySlip2004Image').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPaySlip2007(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPaySlip2007Image').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPaySlip2010(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPaySlip2010Image').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPaySlip2013(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPaySlip2013Image').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPaySlip2016(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPaySlip2016Image').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function uploadPromotionLetter(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#falseinput').attr('src', e.target.result);
            $('#txtPromotionLetterImage').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

const toDataURL = url => fetch(url)
  .then(response => response.blob())
  .then(blob => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.onerror = reject
    reader.readAsDataURL(blob)
}))

function uploadPOI1() {

    var fd = new FormData();

    var files = $('#txtProofOfId')[0].files[0];

    if(files) {
        $(".validpoi .help-block.with-errors").html('');
        $('#txtPOI').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPOI").val(dataUrl);
                      })
                   
                }else{
                    $(".validpoi .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpoi .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Proof of Identity</li></ul>');
    }
}

function uploadSignature1() {

    var fd = new FormData();

    var files = $('#txtSignature')[0].files[0];

    if(files) {
        $(".validsignature .help-block.with-errors").html('');
        $('#txtSignatureCert').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtSignatureCert").val(dataUrl);
                      })
                   
                }else{
                    $(".validsignature .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validsignature .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Signature</li></ul>');
    }
}

function uploadDOB1() {

    var fd = new FormData();

    var files = $('#txtBirthCertificate')[0].files[0];

    if(files) {
        $(".validDOB .help-block.with-errors").html('');
        $('#txtDOBCert').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtDOBCert").val(dataUrl);
                      })
                   
                }else{
                    $(".validDOB .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validDOB .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Birth Certificate</li></ul>');
    }
}

function uploadPassport1() {

    var fd = new FormData();

    var files = $('#txtPassportupload')[0].files[0];

    if(files) {
        $(".validpassport .help-block.with-errors").html('');
        $('#txtPassportImage').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                         $("#txtPassportImage").val(dataUrl);
                      })
                   
                }else{
                    $(".validpassport .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpassport .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Passport</li></ul>');
    }
}

function uploadPOA1() {

    var fd = new FormData();

    var files = $('#txtProofOfAddress')[0].files[0];

    if(files) {
        $(".validcorrespoa .help-block.with-errors").html('');
        $('#txtPOA').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPOA").val(dataUrl);
                      })
                   
                }else{
                    $(".validcorrespoa .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validcorrespoa .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Proof of Address</li></ul>');
    }
}

function uploadEvidenceEmployment1() {

    var fd = new FormData();

    var files = $('#txtEvidenceOfEmployment')[0].files[0];

    if(files) {
        $(".validemployerimage .help-block.with-errors").html('');
        $('#txtEvidenceOfEmploymentImage').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtEvidenceOfEmploymentImage").val(dataUrl);
                      })
                   
                }else{
                    $(".validemployerimage .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validemployerimage .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Evidence of Employment</li></ul>');
    }
}

function uploadTransferImage1() {

    var fd = new FormData();

    var files = $('#txtEmpTransferDoc')[0].files[0];

    if(files) {
        $(".validtransferdoc .help-block.with-errors").html('');
        $('#txtEmpTransferImage').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtEmpTransferImage").val(dataUrl);
                      })
                   
                }else{
                    $(".validtransferdoc .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validtransferdoc .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Transfer Document</li></ul>');
    }
}

function uploadPaySlip20041() {

    var fd = new FormData();

    var files = $('#txtPaySlip2004')[0].files[0];

    if(files) {
        $(".validpay2004 .help-block.with-errors").html('');
        $('#txtPaySlip2004Image').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPaySlip2004Image").val(dataUrl);
                      })
                   
                }else{
                    $(".validpay2004 .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpay2004 .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Image</li></ul>');
    }
}

function uploadPaySlip20071() {

    var fd = new FormData();

    var files = $('#txtPaySlip2007')[0].files[0];

    if(files) {
        $(".validpay2007 .help-block.with-errors").html('');
        $('#txtPaySlip2007Image').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPaySlip2007Image").val(dataUrl);
                      })
                   
                }else{
                    $(".validpay2007 .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpay2007 .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Image</li></ul>');
    }
}

function uploadPaySlip20101() {

    var fd = new FormData();

    var files = $('#txtPaySlip2010')[0].files[0];

    if(files) {
        $(".validpay2010 .help-block.with-errors").html('');
        $('#txtPaySlip2010Image').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPaySlip2010Image").val(dataUrl);
                      })
                   
                }else{
                    $(".validpay2010 .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpay2010 .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Image</li></ul>');
    }
}

function uploadPaySlip20131() {

    var fd = new FormData();

    var files = $('#txtPaySlip2013')[0].files[0];

    if(files) {
        $(".validpay2013 .help-block.with-errors").html('');
        $('#txtPaySlip2013Image').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPaySlip2013Image").val(dataUrl);
                      })
                   
                }else{
                    $(".validpay2013 .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpay2013 .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Image</li></ul>');
    }
}

function uploadPaySlip20161() {

    var fd = new FormData();

    var files = $('#txtPaySlip2016')[0].files[0];

    if(files) {
        $(".validpay2016 .help-block.with-errors").html('');
        $('#txtPaySlip2016Image').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPaySlip2016Image").val(dataUrl);
                      })
                   
                }else{
                    $(".validpay2016 .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpay2016 .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Image</li></ul>');
    }
}

function uploadPromotionLetter1() {

    var fd = new FormData();

    var files = $('#txtPromotionLetter')[0].files[0];

    if(files) {
        $(".validpromotionimage .help-block.with-errors").html('');
        $('#txtPromotionLetterImage').removeAttr('value');

        fd.append('file',files);

        $.ajax({
            url:'ajaximage.php',
            type:'post',
            data:fd,
            contentType: false,
            processData: false,
            success:function(response){
                if(response != 0){

                    toDataURL(response)
                      .then(dataUrl => {
                        // console.log('RESULT:', dataUrl)
                        // console.log(dataUrl)
                         $("#txtPromotionLetterImage").val(dataUrl);
                      })
                   
                }else{
                    $(".validpromotionimage .help-block.with-errors").html('<ul class="list-unstyled"><li>Invalid File Format</li></ul>');
                }
            }
        });
    }
    else {
        $(".validpromotionimage .help-block.with-errors").html('<ul class="list-unstyled"><li>Please upload Image</li></ul>');
    }
}


function nextStep2() {
    var cRecordID = $('#txtRecordID').val();
    var cTitle = $("#ddTitle option:selected").val();
    var cSurname = $("#txtSurname").val();
    var cFirstName = $("#txtFirstName").val();
    var cMiddleName = $("#txtMiddleName").val();
    var cMothersMaidenName = $("#txtMothersMaidenName").val();
    var cMaidenFormer = $("#txtMaidenFormer").val();
    var cGender = $("#ddGender").val();
    var cMaritalStatus = $("#ddMaritalStatus option:selected").val();
    var cDOB = $("#txtDOB").val();
    var cPlaceOfBirth = $("#txtPlaceOfBirth").val();
    var cCountry = $("#ddCountry option:selected").val();
    var cStateOrigin = $("#ddStates option:selected").val();
    var cLGA = $("#ddLGA option:selected").val();
    var cPassport = $("#txtPassport").val();
    var cBVN = $("#txtBVN").val();
    var cNIN = $("#txtNIN").val();
    var cBankName = $("#txtBankName").val();
    var cBankAccountNumber = $("#txtBankAccountNumber").val();
    var cStatementOption = $("#ddStatementOption option:selected").val();
    var cDOBCert = $("#txtDOBCert").val();
    var cPassportImage = $("#txtPassportImage").val();
    var cProofOfID = $("#ddProofOfID option:selected").val();
    var cPOI = $("#txtPOI").val();
    var cSignature = $("#txtSignatureCert").val();
    var cNoSignature = $("#txtNoSignatureCert").val();
    var cSignatureStatus = $("#txtSignatureStatus").val();
    var cNoSignatureStatus = $("#txtNoSignatureStatus").val();
    var cDOBCertStatus = $("#txtDOBCertStatus").val();
    var cPOIStatus = $("#txtPOIStatus").val();

    if(cTitle) 
        $(".validtitle .help-block.with-errors").html('');
    else 
        $(".validtitle .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Title</li></ul>');
    if(cGender) 
        $(".validgender .help-block.with-errors").html('');
    else 
        $(".validgender .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Gender</li></ul>');
    if(cSurname) 
        $(".validsurname .help-block.with-errors").html('');
    else 
        $(".validsurname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter your surname</li></ul>');
    if(cFirstName) 
        $(".validfirstname .help-block.with-errors").html('');
    else 
        $(".validfirstname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter your firstname</li></ul>');
    if(cPlaceOfBirth) 
        $(".validpobirth .help-block.with-errors").html('');
    else 
        $(".validpobirth .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Place of Birth</li></ul>');
    if(cMaritalStatus) 
        $(".validmaritalstatus .help-block.with-errors").html('');
    else 
        $(".validmaritalstatus .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select marital status</li></ul>');
    if(cCountry) 
        $(".validcountry .help-block.with-errors").html('');
    else 
        $(".validcountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Country</li></ul>');
    if(cBVN) {
        if(cBVN.length != 11) {
            $(".validbvn .help-block.with-errors").html('<ul class="list-unstyled"><li>BVN is invalid</li></ul>');
        } else {
            $(".validbvn .help-block.with-errors").html('');
        }
    } else {
        $(".validbvn .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter BVN</li></ul>');
    }
    if(cStatementOption) 
        $(".validoption .help-block.with-errors").html('');
    else 
        $(".validoption .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Statement Option</li></ul>');
    if (cCountry != '566' && cPassport == '') {
        $(".validpassportnumber .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter international passport number</li></ul>');
    }
    else if(cTitle&&cGender&&cSurname&&cFirstName&&cMaritalStatus&&cCountry&&cBVN.length==11&&cStatementOption&&cPlaceOfBirth) { 

        var dataString = 'id=' + cRecordID + '&title=' + cTitle + '&surname=' + cSurname + '&firstname=' + cFirstName + '&middlename=' + cMiddleName + '&mothersmadienname=' + 
        cMothersMaidenName + '&maidenformer=' + cMaidenFormer + '&geneder=' + cGender + '&maritalstatus=' + cMaritalStatus + '&dob=' + cDOB + '&placeofbirth=' + cPlaceOfBirth + 
        '&country=' + cCountry + '&stateorigin=' + cStateOrigin + '&lga=' + cLGA + '&passport=' + cPassport + '&bvn=' + cBVN + '&nin=' + cNIN + '&bankname=' + cBankName + 
        '&bankaccountname=' + cBankAccountNumber + '&birthcert=' + cDOBCert + '&passportimage=' + cPassportImage + '&proofofid=' + cProofOfID + '&proofofidimage=' + cPOI + 
        '&statementoption=' + cStatementOption + '&signature=' + cSignature + '&nosignature=' + cNoSignature;
    
        $.ajax({
            type: "POST",
            url: "ajax.php",
            data: dataString,
            cache: false,
            success: function(html) {
                //alert("html");
                $("#section-1 .help-block.with-errors").html('');
                $("#section-1").removeClass("open");
                $("#section-1").addClass("slide-left");
                $("#section-2").removeClass("slide-right");
                $("#section-2").addClass("open");
            }
        });

        
        $("#section-1 .help-block.with-errors").html('');$("#section-1").removeClass("open");
        $("#section-1").addClass("slide-left");$("#section-2").removeClass("slide-right");
        $("#section-2").addClass("open");        

    } else { 
        $("#section-1 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}

function nextStep2c() {
    var cRecordID = $('#txtRecordID').val();
    var cTitle = $("#ddTitle option:selected").val();
    var cSurname = $("#txtSurname").val();
    var cFirstName = $("#txtFirstName").val();
    var cMiddleName = $("#txtMiddleName").val();
    var cMothersMaidenName = $("#txtMothersMaidenName").val();
    var cMaidenFormer = $("#txtMaidenFormer").val();
    var cGender = $("#ddGender").val();
    var cMaritalStatus = $("#ddMaritalStatus option:selected").val();
    var cDOB = $("#txtDOB").val();
    var cPlaceOfBirth = $("#txtPlaceOfBirth").val();
    var cCountry = $("#ddCountry option:selected").val();
    var cStateOrigin = $("#ddStates option:selected").val();
    var cLGA = $("#ddLGA option:selected").val();
    var cPassport = $("#txtPassport").val();
    var cBVN = $("#txtBVN").val();
    var cNIN = $("#txtNIN").val();
    var cBankName = $("#txtBankName").val();
    var cBankAccountNumber = $("#txtBankAccountNumber").val();
    var cStatementOption = $("#ddStatementOption option:selected").val();
    var cDOBCert = $("#txtDOBCert").val();
    var cPassportImage = $("#txtPassportImage").val();
    var cProofOfID = $("#ddProofOfID option:selected").val();
    var cPOI = $("#txtPOI").val();
    var cSignature = $("#txtSignatureCert").val();
    var cNoSignature = $("#txtNoSignatureCert").val();
    var cSignatureStatus = $("#txtSignatureStatus").val();
    var cNoSignatureStatus = $("#txtNoSignatureStatus").val();
    var cDOBCertStatus = $("#txtDOBCertStatus").val();
    var cPOIStatus = $("#txtPOIStatus").val();

    if(cTitle) 
        $(".validtitle .help-block.with-errors").html('');
    else 
        $(".validtitle .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Title</li></ul>');
    if(cGender) 
        $(".validgender .help-block.with-errors").html('');
    else 
        $(".validgender .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Gender</li></ul>');
    if(cSurname) 
        $(".validsurname .help-block.with-errors").html('');
    else 
        $(".validsurname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter your surname</li></ul>');
    if(cFirstName) 
        $(".validfirstname .help-block.with-errors").html('');
    else 
        $(".validfirstname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter your firstname</li></ul>');
    if(cMaritalStatus) 
        $(".validmaritalstatus .help-block.with-errors").html('');
    else 
        $(".validmaritalstatus .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select marital status</li></ul>');
    if(cCountry) 
        $(".validcountry .help-block.with-errors").html('');
    else 
        $(".validcountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Country</li></ul>');
    if(cBVN) {
        if(cBVN.length != 11) {
            $(".validbvn .help-block.with-errors").html('<ul class="list-unstyled"><li>BVN is invalid</li></ul>');
        } else {
            $(".validbvn .help-block.with-errors").html('');
        }
    } else {
        $(".validbvn .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter BVN</li></ul>');
    }
    if(cStatementOption) 
        $(".validoption .help-block.with-errors").html('');
    else 
        $(".validoption .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Statement Option</li></ul>');

    if (cCountry != '566' && cPassport == '') {
        $(".validpassportnumber .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter international passport number</li></ul>');
    }
    else if(cTitle&&cGender&&cSurname&&cFirstName&&cMaritalStatus&&cCountry&&cBVN.length==11&&cStatementOption) { 

        var dataString = 'id=' + cRecordID + '&title=' + cTitle + '&surname=' + cSurname + '&firstname=' + cFirstName + '&middlename=' + cMiddleName + '&mothersmadienname=' + 
        cMothersMaidenName + '&maidenformer=' + cMaidenFormer + '&geneder=' + cGender + '&maritalstatus=' + cMaritalStatus + '&dob=' + cDOB + '&placeofbirth=' + cPlaceOfBirth + 
        '&country=' + cCountry + '&stateorigin=' + cStateOrigin + '&lga=' + cLGA + '&passport=' + cPassport + '&bvn=' + cBVN + '&nin=' + cNIN + '&bankname=' + cBankName + 
        '&bankaccountname=' + cBankAccountNumber + '&birthcert=' + cDOBCert + '&passportimage=' + cPassportImage + '&proofofid=' + cProofOfID + '&proofofidimage=' + cPOI + 
        '&statementoption=' + cStatementOption + '&signature=' + cSignature + '&nosignature=' + cNoSignature;
    
        $.ajax({
            type: "POST",
            url: "ajax.php",
            data: dataString,
            cache: false,
            success: function(html) {
                //alert("html");
                $("#section-1 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Record Saved Successfully</li></ul>');
            }
        });     

    } else { 
        $("#section-1 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}

function previousStep1() { 
    $("#section-1").removeClass("slide-left"); 
    $("#section-1").addClass("open");$("#section-2").removeClass("open"); 
    $("#section-2").addClass("slide-right");
}

function nextStep3() {
    var cRecordID = $('#txtRecordID').val();
    var cHouseNo = $("#txtHouseNo").val();
    var cResidentialAddress = $("#txtResidentialAddress").val();
    var cResidentialCity = $("#txtResidentialCity").val();
    var cCountry = $("#ddResidentialCountry option:selected").val();
    var cResidentialState = $("#ddResidentialState option:selected").val();
    var cResidentialLGAs = $("#txtResidentialLGAs").val();
    var cResidentialPOBox = $("#txtResidentialPOBOX").val();
    var cResidentialZipCode = $("#txtResidentialZipCode").val();
    var cCorresHouseNo = $("#txtCorresHouseNo").val();
    var cCorressAddress = $("#txtCorressAdderess").val();
    var cCorressTown = $("#txtCorressTown").val();
    var cCorresCountry = $("#ddCorresCountry option:selected").val();
    var cCorressState = $("#ddCorressState option:selected").val();
    var cCorressLGA = $("#ddCorressLGA option:selected").val();
    var cCorressMobile = $("#txtCorressMobile").val();
    var cCorresMobileInt = $("#txtCorresMobileInt").val();
    var cEmail = $("#txtEmail").val();
    var cCorresPOBOX = $("#txtCorresPOBOX").val();
    var cCorresZip = $("#txtCorresZip").val();
    var cProofOfAddress = $("#ddProofOfAddress option:selected").val();
    var cPOA = $("#txtPOA").val();
    var cPOAStatus = $("#txtPOAStatus").val();


    if(cHouseNo) 
        $(".validreshouseno .help-block.with-errors").html('');
    else 
        $(".validreshouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter house no</li></ul>');
    if(cResidentialAddress) 
        $(".validresaddress .help-block.with-errors").html('');
    else 
        $(".validresaddress .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter residential address</li></ul>');
    if(cResidentialCity) 
        $(".validrescity .help-block.with-errors").html('');
    else 
        $(".validrescity .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter village/town/city</li></ul>');
    if(cCountry) 
        $(".validrescountry .help-block.with-errors").html('');
    else 
        $(".validrescountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Country</li></ul>');
    if(cCorresHouseNo) 
        $(".validcorreshouseno .help-block.with-errors").html('');
    else 
        $(".validcorreshouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence house no</li></ul>');
    if(cCorressAddress) 
        $(".validcorresaddress .help-block.with-errors").html('');
    else 
        $(".validcorresaddress .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence address</li></ul>');
    if(cCorressTown) 
        $(".validcorrestown .help-block.with-errors").html('');
    else 
        $(".validcorrestown .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence town</li></ul>');
    if(cCorresCountry) 
        $(".validcorrescountry .help-block.with-errors").html('');
    else 
        $(".validcorrescountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please correspondence select Country</li></ul>');
    if(cCorressMobile) 
        $(".validcorresmobile .help-block.with-errors").html('');
    else 
        $(".validcorresmobile .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence Mobile</li></ul>');
    if(cEmail) 
        $(".validemail .help-block.with-errors").html('');
    else 
        $(".validemail .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter email</li></ul>');
    if (cCountry != '566' && cResidentialZipCode == '')  {
        $(".validreszipcode .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Residential Zip Code</li></ul>');
    } 
    else if (cCorresCountry != '566' && cCorresZip == '') { 
        $(".validreszipcode .help-block.with-errors").html('');       
        $(".validcorreszipcode .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Correspondence Zip Code</li></ul>');
    } 
    else if(cHouseNo&&cResidentialAddress&&cResidentialCity&&cCountry&&cCorresHouseNo&&cCorressAddress&&cCorressTown&&cCorresCountry&&cCorressMobile) { 
        $(".validreszipcode .help-block.with-errors").html('');
        $(".validcorreszipcode .help-block.with-errors").html('');

        var dataString = 'id=' + cRecordID + '&houseno=' + cHouseNo + '&residentialaddress=' + cResidentialAddress + '&residentialcity=' + cResidentialCity + '&residentialcountry=' + cCountry + '&residentialstate=' + cResidentialState + 
        '&residentiallga=' + cResidentialLGAs + '&residentialpobox=' + cResidentialPOBox + '&residentialzipcode=' + cResidentialZipCode + '&correspondencehouseno=' + cCorresHouseNo + '&correspondenceaddress=' + cCorressAddress + 
        '&correspondencetown=' + cCorressTown + '&correspondencecountry=' + cCorresCountry + '&correspondencestate=' + cCorressState + '&correspondencelga=' + cCorressLGA + '&correspondencemobile=' + cCorressMobile + '&correspondencemobileint=' 
        + cCorresMobileInt + '&email=' + cEmail + '&correspondencepobox=' + cCorresPOBOX + '&correszip=' + cCorresZip + '&proofofaddress=' + cProofOfAddress + '&poa=' + cPOA;

        $.ajax({
            type: "POST",
            url: "ajax2.php",
            data: dataString,
            cache: false,
            success: function(html) {
                //alert("html");
                $("#section-2 .help-block.with-errors.mandatory-error").html('');
                $("#section-2").removeClass("open");$("#section-2").addClass("slide-left");
                $("#section-3").removeClass("slide-right");$("#section-3").addClass("open");
            }
        });


    } else { 
        $("#section-2 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}


function nextStep3c() {
    var cRecordID = $('#txtRecordID').val();
    var cHouseNo = $("#txtHouseNo").val();
    var cResidentialAddress = $("#txtResidentialAddress").val();
    var cResidentialCity = $("#txtResidentialCity").val();
    var cCountry = $("#ddResidentialCountry option:selected").val();
    var cResidentialState = $("#ddResidentialState option:selected").val();
    var cResidentialLGAs = $("#txtResidentialLGAs").val();
    var cResidentialPOBox = $("#txtResidentialPOBOX").val();
    var cResidentialZipCode = $("#txtResidentialZipCode").val();
    var cCorresHouseNo = $("#txtCorresHouseNo").val();
    var cCorressAddress = $("#txtCorressAdderess").val();
    var cCorressTown = $("#txtCorressTown").val();
    var cCorresCountry = $("#ddCorresCountry option:selected").val();
    var cCorressState = $("#ddCorressState option:selected").val();
    var cCorressLGA = $("#ddCorressLGA option:selected").val();
    var cCorressMobile = $("#txtCorressMobile").val();
    var cCorresMobileInt = $("#txtCorresMobileInt").val();
    var cEmail = $("#txtEmail").val();
    var cCorresPOBOX = $("#txtCorresPOBOX").val();
    var cCorresZip = $("#txtCorresZip").val();
    var cProofOfAddress = $("#ddProofOfAddress option:selected").val();
    var cPOA = $("#txtPOA").val();
    var cPOAStatus = $("#txtPOAStatus").val();


    if(cHouseNo) 
        $(".validreshouseno .help-block.with-errors").html('');
    else 
        $(".validreshouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter house no</li></ul>');
    if(cResidentialAddress) 
        $(".validresaddress .help-block.with-errors").html('');
    else 
        $(".validresaddress .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter residential address</li></ul>');
    if(cResidentialCity) 
        $(".validrescity .help-block.with-errors").html('');
    else 
        $(".validrescity .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter village/town/city</li></ul>');
    if(cCountry) 
        $(".validrescountry .help-block.with-errors").html('');
    else 
        $(".validrescountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Country</li></ul>');
    if(cCorresHouseNo) 
        $(".validcorreshouseno .help-block.with-errors").html('');
    else 
        $(".validcorreshouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence house no</li></ul>');
    if(cCorressAddress) 
        $(".validcorresaddress .help-block.with-errors").html('');
    else 
        $(".validcorresaddress .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence address</li></ul>');
    if(cCorressTown) 
        $(".validcorrestown .help-block.with-errors").html('');
    else 
        $(".validcorrestown .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence town</li></ul>');
    if(cCorresCountry) 
        $(".validcorrescountry .help-block.with-errors").html('');
    else 
        $(".validcorrescountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please correspondence select Country</li></ul>');
    if(cCorressMobile) 
        $(".validcorresmobile .help-block.with-errors").html('');
    else 
        $(".validcorresmobile .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter correspondence Mobile</li></ul>');
    if(cEmail) 
        $(".validemail .help-block.with-errors").html('');
    else 
        $(".validemail .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter email</li></ul>');

    if(cHouseNo&&cResidentialAddress&&cResidentialCity&&cCountry&&cCorresHouseNo&&cCorressAddress&&cCorressTown&&cCorresCountry&&cCorressMobile) { 

        var dataString = 'id=' + cRecordID + '&houseno=' + cHouseNo + '&residentialaddress=' + cResidentialAddress + '&residentialcity=' + cResidentialCity + '&residentialcountry=' + cCountry + '&residentialstate=' + cResidentialState + 
        '&residentiallga=' + cResidentialLGAs + '&residentialpobox=' + cResidentialPOBox + '&residentialzipcode=' + cResidentialZipCode + '&correspondencehouseno=' + cCorresHouseNo + '&correspondenceaddress=' + cCorressAddress + 
        '&correspondencetown=' + cCorressTown + '&correspondencecountry=' + cCorresCountry + '&correspondencestate=' + cCorressState + '&correspondencelga=' + cCorressLGA + '&correspondencemobile=' + cCorressMobile + '&correspondencemobileint=' 
        + cCorresMobileInt + '&email=' + cEmail + '&correspondencepobox=' + cCorresPOBOX + '&correszip=' + cCorresZip + '&proofofaddress=' + cProofOfAddress + '&poa=' + cPOA;

        $.ajax({
            type: "POST",
            url: "ajax2.php",
            data: dataString,
            cache: false,
            success: function(html) {
                //alert("html");
                $("#section-2 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Record Saved Successfully</li></ul>');
            }
        });


    } else { 
        $("#section-2 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}

function previousStep2() { 
    $("#section-2").removeClass("slide-left");
    $("#section-2").addClass("open");
    $("#section-3").removeClass("open");
    $("#section-3").addClass("slide-right");
}

function submitCFIForm() {
    var cRecordID = $('#txtRecordID').val();
    var cConfirmation = $('#txtConfirmation').val();
    var cStatus = 2;
    var cPIN = $('#txtUsersPIN').val();

    var cSignatureStatus = $("#txtSignatureStatus").val();
    var cNoSignatureStatus = $("#txtNoSignatureStatus").val();
    var cDOBCertStatus = $("#txtDOBCertStatus").val();
    var cPOIStatus = $("#txtPOIStatus").val();
    var cPOAStatus = $("#txtPOAStatus").val();
    var cEmpImageStatus = $("#txtEmpImageStatus").val();
    if ($('#txtConfirmation').is(':checked')) {
        if (((cSignatureStatus == 1)||(cNoSignatureStatus == 1))&&(cDOBCertStatus == 1)&&(cPOIStatus &= 1)&&(cPOAStatus == 1)&&(cEmpImageStatus == 1)) {
            var dataString = 'id=' + cRecordID + '&status=' + cStatus;
            $.ajax({
                type: "POST",
                url: "ajax5.php",
                data: dataString,
                cache: false,
                success: function(html) {
                    $("#txtFinalBack").hide();
                    $("#txtFinalSubmit").hide();
                    $("#onSubmit").html('<ul class="list-unstyled" style="color:green; font-size:30px;"><li><strong>Records was submitted successfully</strong></li></ul>');
                }
            });
        } else {
            $("#onSubmit").html('<ul class="list-unstyled" style="font-size: 20px;"><li><strong>Kindly ensure all mandatory supporting documents are uploaded</strong></li></ul>');    
        }
    } else {
        $("#onSubmit").html('<ul class="list-unstyled" style="font-size: 20px;"><li><strong>Please ensure you agree to the terms and condition</strong></li></ul>');
    }

}

function nextStep4() {
    var cRecordID = $('#txtRecordID').val();
    var cEmpSector = $("#ddEmpSector option:selected").val();
    var cEmpName = $("#txtEmpName").val();
    var cEmpBusinessName = $("#txEmpBusinessName").val();
    var cEmpID = $("#txtEmpID").val();
    var cEmpHouseNo = $("#txtEmpHouseNo").val();
    var cEmpAddress = $("#txtEmpAddress").val();
    var cEmpCity = $("#txtEmpCity").val();
    var cEmpCountry = $("#ddEmpCountry option:selected").val();
    var cEmpStates = $("#ddEmpStates option:selected").val();
    var cEmpLGA = $("#ddEmpLGA option:selected").val();
    var cNatureOfBusiness = $("#ddNatureOfBusiness option:selected").val();
    var cDesignation = $("#txtDesignation").val();
    var cEmpStaffID = $("#txtEmpStaffID").val();
    var cOfficeNumber = $("#txtOfficeNumber").val();
    var cEmpMobileNoInt = $("#txtEmpMobileNoInt").val();
    var cEmpEmailAddress = $("#txtEmpEmailAddress").val();
    var cZipCode = $("#txtZipCode").val();
    var cEmployeeContribution = $("#txtEmployeeContribution").val();
    var cEmployerContribution = $("#txtEmployerContribution").val();
    var cDateOfFirstEmployment = $("#txtDateOfFirstEmployment").val();
    var cDateOfRetirement = $("#txtDateOfRetirement").val();
    var cDateOfCurrentAppointmentPublic = $("#txtDateOfCurrentAppointmentPublic").val();
    var cEmpServiceNo = $("#txtEmpServiceNo").val();    
    var cDateOfTransferOfService = $("#txtDateOfTransferOfService").val();
    var cEmpPOBox = $("#txtEmpPOBox").val();
    var cEvidenceOfEmployment = $("#ddEvidenceOfEmployment option:selected").val();
    var cEvidenceOfEmploymentImage = $("#txtEvidenceOfEmploymentImage").val();
    var cIPPIS = $("#ddIPPIS option:selected").val();
    var cDateJoinedIPPIS = $("#txtDateJoinedIPPIS").val();

    var cEmpTransferImage = $("#txtEmpTransferImage").val();
    var cPaySlip2004Image = $("#txtPaySlip2004Image").val();
    var cPaySlip2007Image = $("#txtPaySlip2007Image").val();
    var cPaySlip2010Image = $("#txtPaySlip2010Image").val();
    var cPaySlip2013Image = $("#txtPaySlip2013Image").val();
    var cPaySlip2016Image = $("#txtPaySlip2016Image").val();
    var cPromotionLetterImage = $("#txtPromotionLetterImage").val();

    var cHarmonised2004 = $("#ddHarmonised2004 option:selected").val();
    var cGLJune2004 = $("#ddGLJune2004 option:selected").val();
    var cStep2004 = $("#ddStep2004 option:selected").val();
    var cHarmonised2007 = $("#ddHarmonised2007 option:selected").val();
    var cGLJune2007 = $("#ddGLJune2007 option:selected").val();
    var cStep2007 = $("#ddStep2007 option:selected").val();
    var cHarmonised2010 = $("#ddHarmonised2010 option:selected").val();
    var cGLJune2010 = $("#ddGLJune2010 option:selected").val();
    var cStep2010 = $("#ddStep2010 option:selected").val();
    
    var cHarmonised2013 = $("#ddHarmonised2013 option:selected").val();
    var cGLJune2013 = $("#ddGLJune2013 option:selected").val();
    var cStep2013 = $("#ddStep2013 option:selected").val();
    var cHarmonised2016 = $("#ddHarmonised2016 option:selected").val();
    var cGLJune2016 = $("#ddGLJune2016 option:selected").val();
    var cStep2016 = $("#ddStep2016 option:selected").val();

    var cCurrentHarmonised = $("#ddCurrentHarmonised option:selected").val();
    var cCurrentGL = $("#ddCurrentGL option:selected").val();
    var cCurrentStep = $("#ddCurrentStep option:selected").val();

    var cEmpImageStatus = $("#txtEmpImageStatus").val();

    if(cEmpSector) 
        $(".validempsector .help-block.with-errors").html('');
    else 
        $(".validempsector .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Sector Classification</li></ul>');
    if(cEmpName) 
        $(".validempname .help-block.with-errors").html('');
    else 
        $(".validempname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Employer Name</li></ul>');
    if(cEmpBusinessName) 
        $(".validempcode .help-block.with-errors").html('');
    else 
        $(".validempcode .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Employer Business Name</li></ul>');
    if(cEmpHouseNo) 
        $(".validemphouseno .help-block.with-errors").html('');
    else 
        $(".validemphouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter house number</li></ul>');
    if(cEmpAddress) 
        $(".validempstreet .help-block.with-errors").html('');
    else 
        $(".validempstreet .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter street address</li></ul>');
    if(cEmpStaffID) 
        $(".validstaffid .help-block.with-errors").html('');
    else 
        $(".validstaffid .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Staff ID</li></ul>');
    if(cEmpCity) 
        $(".validemptown .help-block.with-errors").html('');
    else 
        $(".validemptown .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter city</li></ul>');
    if(cEmpCountry) 
        $(".validempcountry .help-block.with-errors").html('');
    else 
        $(".validempcountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select country</li></ul>');
    if(cDateOfFirstEmployment) 
        $(".validempdateoffirstemp .help-block.with-errors").html('');
    else 
        $(".validempdateoffirstemp .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter date of first employment</li></ul>');
    if (cDateOfFirstEmployment <= '1971-01-01') {        
        $(".validempdateoffirstemp .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter a valid date</li></ul>');
    } 
    else if (cDateOfFirstEmployment > cDateOfCurrentAppointmentPublic) {  
        $(".validempdateoffirstemp .help-block.with-errors").html('');    

        $(".validcurrentappointment .help-block.with-errors").html('<ul class="list-unstyled"><li>Date can not be lesser than date of employment</li></ul>');
    } 
    else if (cDateOfFirstEmployment > cDateOfRetirement) {
        $(".validempdateoffirstemp .help-block.with-errors").html('');        
        $(".validretirement .help-block.with-errors").html('<ul class="list-unstyled"><li>Date can not be greater than date of employment</li></ul>');
    }
    else if (cEmpCountry != '566' && cZipCode == '')  {
        $(".validempdateoffirstemp .help-block.with-errors").html(''); 
        $(".validcurrentappointment .help-block.with-errors").html(''); 
        $(".validretirement .help-block.with-errors").html('');

        $(".validempzipcode .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Employer Zip Code</li></ul>');
    } 
    else if(cEmpSector&&cEmpName&&cEmpHouseNo&&cEmpAddress&&cEmpCity&&cEmpCountry&&cDateOfFirstEmployment&&cEmpStaffID&&cEmpBusinessName) { 
        $(".validempzipcode .help-block.with-errors").html('');

        var dataString = 'id=' + cRecordID + '&empsector=' + cEmpSector + '&empname=' + cEmpName + '&empid=' + cEmpID + '&emphouseno=' + cEmpHouseNo + '&empaddress=' + cEmpAddress + '&empcity=' + cEmpCity + 
        '&empcountry=' + cEmpCountry + '&empstates=' + cEmpStates + '&emplga=' + cEmpLGA + '&natureofbusiness=' + cNatureOfBusiness + '&designation=' + cDesignation + '&emptstaffid=' + cEmpStaffID + 
        '&officenumber=' + cOfficeNumber + '&empmobilenoint=' + cEmpMobileNoInt + '&empemailaddress=' + cEmpEmailAddress + '&zipcode=' + cZipCode + '&dateoffirstemployment=' + 
        cDateOfFirstEmployment + '&dateofretirement=' + cDateOfRetirement + '&dateofcurrentappointpublic=' + cDateOfCurrentAppointmentPublic + '&empserviceno=' + cEmpServiceNo + 
        '&dateoftransferofservice=' + cDateOfTransferOfService + '&emppobox=' + cEmpPOBox + '&evidenceofemployment=' + cEvidenceOfEmployment + '&evidenceofemploymentimage=' + cEvidenceOfEmploymentImage +
        '&ippis=' + cIPPIS + '&datejoinedippis=' + cDateJoinedIPPIS + '&hamonised2004=' + cHarmonised2004 + '&gljune2004=' + cGLJune2004 + '&step2004=' + cStep2004 + '&harmonised2007=' + cHarmonised2007 + 
        '&gljune2007=' + cGLJune2007 + '&step2007=' + cStep2007 + '&harmonised2010=' + cHarmonised2010 + '&gljune2010=' + cGLJune2010 + '&step2010=' + cStep2010 + '&harmonise2013=' + cHarmonised2013 + 
        '&june2013=' + cGLJune2013 + '&step2013=' + cStep2013 + '&harmonise2016=' + cHarmonised2016 + '&june2016=' + cGLJune2016 + '&step2016=' + cStep2016 + '&currentharmonised=' + cCurrentHarmonised +
        '&currentgl=' + cCurrentGL + '&currentstep=' + cCurrentStep + '&emptransferimage=' + cEmpTransferImage + '&payslip2004=' + cPaySlip2004Image  + '&payslip2007=' + cPaySlip2007Image + '&payslip2010=' + 
        cPaySlip2010Image + '&payslip2013=' + cPaySlip2013Image + '&payslip2016=' + cPaySlip2016Image + '&promotionletter=' + cPromotionLetterImage + '&empbusinessname=' + cEmpBusinessName + 
        '&employeecontribution=' + cEmployeeContribution + '&employercontribution=' + cEmployerContribution;

        $.ajax({
            type: "POST",
            url: "ajax3.php",
            data: dataString,
            cache: false,
            success: function(html) {
                //alert("html");
                $("#section-3 .help-block.with-errors.mandatory-error").html('');
                $("#section-3").removeClass("open");$("#section-3").addClass("slide-left");
                $("#section-4").removeClass("slide-right");$("#section-4").addClass("open");
            }
        });

    } else { 
        $("#section-3 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}

function nextStep4c() {
    var cRecordID = $('#txtRecordID').val();
    var cEmpSector = $("#ddEmpSector option:selected").val();
    var cEmpName = $("#txtEmpName").val();
    var cEmpBusinessName = $("#txEmpBusinessName").val();
    var cEmpID = $("#txtEmpID").val();
    var cEmpHouseNo = $("#txtEmpHouseNo").val();
    var cEmpAddress = $("#txtEmpAddress").val();
    var cEmpCity = $("#txtEmpCity").val();
    var cEmpCountry = $("#ddEmpCountry option:selected").val();
    var cEmpStates = $("#ddEmpStates option:selected").val();
    var cEmpLGA = $("#ddEmpLGA option:selected").val();
    var cNatureOfBusiness = $("#ddNatureOfBusiness option:selected").val();
    var cDesignation = $("#txtDesignation").val();
    var cEmpStaffID = $("#txtEmpStaffID").val();
    var cOfficeNumber = $("#txtOfficeNumber").val();
    var cEmpMobileNoInt = $("#txtEmpMobileNoInt").val();
    var cEmpEmailAddress = $("#txtEmpEmailAddress").val();
    var cZipCode = $("#txtZipCode").val();
    var cEmployeeContribution = $("#txtEmployeeContribution").val();
    var cEmployerContribution = $("#txtEmployerContribution").val();
    var cDateOfFirstEmployment = $("#txtDateOfFirstEmployment").val();
    var cDateOfRetirement = $("#txtDateOfRetirement").val();
    var cDateOfCurrentAppointmentPublic = $("#txtDateOfCurrentAppointmentPublic").val();
    var cEmpServiceNo = $("#txtEmpServiceNo").val();    
    var cDateOfTransferOfService = $("#txtDateOfTransferOfService").val();
    var cEmpPOBox = $("#txtEmpPOBox").val();
    var cEvidenceOfEmployment = $("#ddEvidenceOfEmployment option:selected").val();
    var cEvidenceOfEmploymentImage = $("#txtEvidenceOfEmploymentImage").val();
    var cIPPIS = $("#ddIPPIS option:selected").val();
    var cDateJoinedIPPIS = $("#txtDateJoinedIPPIS").val();

    var cEmpTransferImage = $("#txtEmpTransferImage").val();
    var cPaySlip2004Image = $("#txtPaySlip2004Image").val();
    var cPaySlip2007Image = $("#txtPaySlip2007Image").val();
    var cPaySlip2010Image = $("#txtPaySlip2010Image").val();
    var cPaySlip2013Image = $("#txtPaySlip2013Image").val();
    var cPaySlip2016Image = $("#txtPaySlip2016Image").val();
    var cPromotionLetterImage = $("#txtPromotionLetterImage").val();

    var cHarmonised2004 = $("#ddHarmonised2004 option:selected").val();
    var cGLJune2004 = $("#ddGLJune2004 option:selected").val();
    var cStep2004 = $("#ddStep2004 option:selected").val();
    var cHarmonised2007 = $("#ddHarmonised2007 option:selected").val();
    var cGLJune2007 = $("#ddGLJune2007 option:selected").val();
    var cStep2007 = $("#ddStep2007 option:selected").val();
    var cHarmonised2010 = $("#ddHarmonised2010 option:selected").val();
    var cGLJune2010 = $("#ddGLJune2010 option:selected").val();
    var cStep2010 = $("#ddStep2010 option:selected").val();

    var cHarmonised2013 = $("#ddHarmonised2013 option:selected").val();
    var cGLJune2013 = $("#ddGLJune2013 option:selected").val();
    var cStep2013 = $("#ddStep2013 option:selected").val();
    var cHarmonised2016 = $("#ddHarmonised2016 option:selected").val();
    var cGLJune2016 = $("#ddGLJune2016 option:selected").val();
    var cStep2016 = $("#ddStep2016 option:selected").val();

    var cCurrentHarmonised = $("#ddCurrentHarmonised option:selected").val();
    var cCurrentGL = $("#ddCurrentGL option:selected").val();
    var cCurrentStep = $("#ddCurrentStep option:selected").val();

    var cEmpImageStatus = $("#txtEmpImageStatus").val();

    if(cEmpSector) 
        $(".validempsector .help-block.with-errors").html('');
    else 
        $(".validempsector .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select Sector Classification</li></ul>');
    if(cEmpName) 
        $(".validempname .help-block.with-errors").html('');
    else 
        $(".validempname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Employer Name</li></ul>');
    if(cEmpID) 
        $(".validempcode .help-block.with-errors").html('');
    else 
        $(".validempcode .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter Employer Code</li></ul>');
    if(cEmpHouseNo) 
        $(".validemphouseno .help-block.with-errors").html('');
    else 
        $(".validemphouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter house number</li></ul>');
    if(cEmpAddress) 
        $(".validempstreet .help-block.with-errors").html('');
    else 
        $(".validempstreet .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter street address</li></ul>');
    if(cEmpCity) 
        $(".validemptown .help-block.with-errors").html('');
    else 
        $(".validemptown .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter city</li></ul>');
    if(cEmpCountry) 
        $(".validempcountry .help-block.with-errors").html('');
    else 
        $(".validempcountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select country</li></ul>');
    if(cDateOfFirstEmployment) 
        $(".validempdateoffirstemp .help-block.with-errors").html('');
    else 
        $(".validempdateoffirstemp .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter date of first employment</li></ul>');
    
    if(cEmpSector&&cEmpName&&cEmpID&&cEmpHouseNo&&cEmpAddress&&cEmpCity&&cEmpCountry&&cDateOfFirstEmployment) { 

        var dataString = 'id=' + cRecordID + '&empsector=' + cEmpSector + '&empname=' + cEmpName + '&empid=' + cEmpID + '&emphouseno=' + cEmpHouseNo + '&empaddress=' + cEmpAddress + '&empcity=' + cEmpCity + 
        '&empcountry=' + cEmpCountry + '&empstates=' + cEmpStates + '&emplga=' + cEmpLGA + '&natureofbusiness=' + cNatureOfBusiness + '&designation=' + cDesignation + '&emptstaffid=' + cEmpStaffID + 
        '&officenumber=' + cOfficeNumber + '&empmobilenoint=' + cEmpMobileNoInt + '&empemailaddress=' + cEmpEmailAddress + '&zipcode=' + cZipCode + '&dateoffirstemployment=' + 
        cDateOfFirstEmployment + '&dateofretirement=' + cDateOfRetirement + '&dateofcurrentappointpublic=' + cDateOfCurrentAppointmentPublic + '&empserviceno=' + cEmpServiceNo + 
        '&dateoftransferofservice=' + cDateOfTransferOfService + '&emppobox=' + cEmpPOBox + '&evidenceofemployment=' + cEvidenceOfEmployment + '&evidenceofemploymentimage=' + cEvidenceOfEmploymentImage +
        '&ippis=' + cIPPIS + '&datejoinedippis=' + cDateJoinedIPPIS + '&hamonised2004=' + cHarmonised2004 + '&gljune2004=' + cGLJune2004 + '&step2004=' + cStep2004 + '&harmonised2007=' + cHarmonised2007 + 
        '&gljune2007=' + cGLJune2007 + '&step2007=' + cStep2007 + '&harmonised2010=' + cHarmonised2010 + '&gljune2010=' + cGLJune2010 + '&step2010=' + cStep2010 + '&harmonise2013=' + cHarmonised2013 + 
        '&june2013=' + cGLJune2013 + '&step2013=' + cStep2013 + '&harmonise2016=' + cHarmonised2016 + '&june2016=' + cGLJune2016 + '&step2016=' + cStep2016 + '&currentharmonised=' + cCurrentHarmonised +
        '&currentgl=' + cCurrentGL + '&currentstep=' + cCurrentStep + '&emptransferimage=' + cEmpTransferImage + '&payslip2004=' + cPaySlip2004Image  + '&payslip2007=' + cPaySlip2007Image + '&payslip2010=' + 
        cPaySlip2010Image + '&payslip2013=' + cPaySlip2013Image + '&payslip2016=' + cPaySlip2016Image + '&promotionletter=' + cPromotionLetterImage + '&empbusinessname=' + cEmpBusinessName + 
        '&employeecontribution=' + cEmployeeContribution + '&employercontribution=' + cEmployerContribution;

        $.ajax({
            type: "POST",
            url: "ajax3.php",
            data: dataString,
            cache: false,
            success: function(html) {
                $("#section-3 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Record Saved Successfully</li></ul>');
            }
        });


    } else { 
        $("#section-3 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}

function previousStep3() { 
    $("#section-3").removeClass("slide-left");
    $("#section-3").addClass("open");$("#section-4").removeClass("open");
    $("#section-4").addClass("slide-right");
}

function nextStep5() {
    var cRecordID = $('#txtRecordID').val();
    var cNOKTitle = $("#ddNOKTitle option:selected").val();
    var cNOKGender = $("#ddNOKGender option:selected").val();
    var cNOKRelationship = $("#ddNOKRelationship option:selected").val();
    var cNOKSurname = $("#txtNOKSurname").val();
    var cNOKFirstName = $("#txtNOKFirstName").val();
    var cNOKMiddleName = $("#txtNOKMiddleName").val();
    var cNOKHouseNo = $("#txtNOKHouseNo").val();
    var cNOKStreetAddress = $("#txtNOKStreetAddress").val();
    var cNOKTown = $("#txtNOKCity").val();
    var cNOKCountry = $("#ddNOKCountry option:selected").val();
    var cNOKStates = $("#ddNOKStates option:selected").val();
    var cNOKLGAs = $("#ddNOKLGAs option:selected").val();
    var cNOKMobileNo = $("#txtNOKMobileNo").val();
    var cNOKMobileNoInt = $("#txtNOKMobileNoInt").val();
    var cNOKEmail = $("#txtNOKEmail").val();
    var cNOKPOBox = $("#txtNOKPOBox").val();
    var cNOKZipCode = $("#txtNOKZipCode").val();

    var cBENTitle = $("#ddBENTitle").val();
    var cBENGender = $("#ddBENGender").val();
    var cBENRelationship = $("#ddBENRelationship").val();
    var cBENSurname = $("#txtBENSurname").val();
    var cBENFirstName = $("#txtBENFirstName").val();
    var cBENMiddleName = $("#txtBENMiddleName").val();
    var cBENHouseNo = $("#txtBENHouseNo").val();
    var cBENStreetAddress = $("#txtBENStreetAddress").val();
    var cBENTown = $("#txtBENCity").val();
    var cBENCountry = $("#ddBENCountry").val();
    var cBENStates = $("#ddBENStates").val();
    var cBENLGAs = $("#ddBENLGAs").val();
    var cBENMobileNo = $("#txtBENMobileNo").val();
    var cBENMobileNoInt = $("#txtBENMobileNoInt").val();
    var cBENEmail = $("#txtBENEmail").val();
    var cBENPOBox = $("#txtBENPOBox").val();
    var cBENZipCode = $("#txtBENZipCode").val();

    if(cNOKTitle) 
        $(".validnoktittle .help-block.with-errors").html('');
    else 
        $(".validnoktittle .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select title</li></ul>');
    if(cNOKGender) 
        $(".validnokgender .help-block.with-errors").html('');
    else 
        $(".validnokgender .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select gender</li></ul>');
    if(cNOKRelationship) 
        $(".validnokrelationship .help-block.with-errors").html('');
    else 
        $(".validnokrelationship .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select relationship</li></ul>');
    if(cNOKSurname) 
        $(".validnoksurname .help-block.with-errors").html('');
    else 
        $(".validnoksurname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter surname</li></ul>');
    if(cNOKFirstName) 
        $(".validnokfirstname .help-block.with-errors").html('');
    else 
        $(".validnokfirstname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter first name</li></ul>');
    if(cNOKHouseNo) 
        $(".validnokhouseno .help-block.with-errors").html('');
    else 
        $(".validnokhouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter house number</li></ul>');
    if(cNOKStreetAddress) 
        $(".validnokstreetaddress .help-block.with-errors").html('');
    else 
        $(".validnokstreetaddress .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter street address</li></ul>');
    if(cNOKTown) 
        $(".validnokcity .help-block.with-errors").html('');
    else 
        $(".validnokcity .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter town</li></ul>');
    if(cNOKCountry) 
        $(".validnokcountry .help-block.with-errors").html('');
    else 
        $(".validnokcountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select country</li></ul>');
    if(cNOKStates) 
        $(".validnokstate .help-block.with-errors").html('');
    else 
        $(".validnokstate .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select state</li></ul>');
    if(cNOKLGAs) 
        $(".validnoklga .help-block.with-errors").html('');
    else 
        $(".validnoklga .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select local government area</li></ul>');
    if(cNOKMobileNo) 
        $(".validnokmobile .help-block.with-errors").html('');
    else 
        $(".validnokmobile .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select mobile number</li></ul>');

    if (cNOKCountry != '566' && cNOKZipCode == '')  {
        $(".validempzipcode .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter NOK Zip Code</li></ul>');
    } 
    else if(cNOKTitle&&cNOKGender&&cNOKRelationship&&cNOKSurname&&cNOKFirstName&&cNOKHouseNo&&cNOKStreetAddress&&cNOKTown&&cNOKCountry&&cNOKStates&&cNOKLGAs&&cNOKMobileNo) { 
        $(".validempzipcode .help-block.with-errors").html('');
        $(".validbencorresszip .help-block.with-errors").html('');

        var dataString = 'id=' + cRecordID + '&noktitle=' + cNOKTitle + '&nokgender=' + cNOKGender + '&nokrelationship=' + cNOKRelationship + '&noksurname=' + cNOKSurname + '&nokfirstname=' + cNOKFirstName + '&nokmiddlename=' + cNOKMiddleName + 
        '&nokhouseno=' + cNOKHouseNo + '&nokstreetaddress=' + cNOKStreetAddress + '&nokcountry=' + cNOKCountry + '&nokstates=' + cNOKStates + '&noklgas=' + cNOKLGAs + '&noktown=' + cNOKTown + '&nokmobileno=' + 
        cNOKMobileNo + '&nokmobilenoint=' + cNOKMobileNoInt + '&nokemail=' + cNOKEmail + '&nokpobox=' + cNOKPOBox + '&nokzipcode=' + cNOKZipCode + '&bentitle=' + cBENTitle + '&bengender=' +
         cBENGender + '&benrelationship=' + cBENRelationship + '&bensurname=' + cBENSurname + '&benfirstname=' + cBENFirstName + '&benmiddlename=' + cBENMiddleName + '&benhouseno=' + cBENHouseNo + 
         '&benstreetaddress=' + cBENStreetAddress + '&bentown=' + cBENTown + '&bencountry=' + cBENCountry + '&benstate=' + cBENStates + '&benlga=' + cBENLGAs + '&benmobile=' + cBENMobileNo +
          '&benmobileint=' + cBENMobileNoInt + '&benemail=' + cBENEmail + '&benpobox=' + cBENPOBox + '&benzipcode=' + cBENZipCode;

        $.ajax({
            type: "POST",
            url: "ajax4.php",
            data: dataString,
            cache: false,
            success: function(html) {

                var cTitle = $("#ddTitle option:selected").val();
                var cSurname = $("#txtSurname").val();
                var cFirstName = $("#txtFirstName").val();
                var cMiddleName = $("#txtMiddleName").val();
                var cMothersMaidenName = $("#txtMaidenFormer").val();
                var cGender = $("#ddGender").val();
                var cMaritalStatus = $("#ddMaritalStatus option:selected").val();
                var cDOB = $("#txtDOB").val();
                var cPlaceOfBirth = $("#txtPlaceOfBirth").val();
                var cCountry = $("#ddCountry option:selected").val();
                var cStateOrigin = $("#ddStates option:selected").val();
                var cLGA = $("#ddLGA option:selected").val();
                var cPassport = $("#txtPassport").val();
                var cBVN = $("#txtBVN").val();
                var cNIN = $("#txtNIN").val();

                var cHouseNo = $("#txtHouseNo").val();
                var cResidentialAddress = $("#txtResidentialAddress").val();
                var cResidentialCity = $("#txtResidentialCity").val();
                var cResidentialCountry = $("#ddResidentialCountry option:selected").val();
                var cResidentialState = $("#ddResidentialState option:selected").val();
                var cResidentialLGAs = $("#txtResidentialLGAs").val();
                var cResidentialPOBox = $("#txtResidentialPOBOX").val();
                var cResidentialZipCode = $("#txtResidentialZipCode").val();
                var cCorresHouseNo = $("#txtCorresHouseNo").val();
                var cCorressAddress = $("#txtCorressAdderess").val();
                var cCorressTown = $("#txtCorressTown").val();
                var cCorresCountry = $("#ddCorresCountry option:selected").val();
                var cCorressState = $("#ddCorressState option:selected").val();
                var cCorressLGA = $("#ddCorressLGA option:selected").val();
                var cCorressMobile = $("#txtCorressMobile").val();
                var cCorresMobileInt = $("#txtCorresMobileInt").val();
                var cEmail = $("#txtEmail").val();
                var cCorresPOBOX = $("#txtCorresPOBOX").val();
                var cCorresZip = $("#txtCorresZip").val();

                var cEmpSector = $("#ddEmpSector option:selected").val();
                var cEmpName = $("#txtEmpName").val();
                var cEmpID = $("#txtEmpID").val();
                var cEmpHouseNo = $("#txtEmpHouseNo").val();
                var cEmpAddress = $("#txtEmpAddress").val();
                var cEmpCity = $("#txtEmpCity").val();
                var cEmpCountry = $("#ddEmpCountry option:selected").val();
                var cEmpStates = $("#ddEmpStates option:selected").val();
                var cEmpLGA = $("#ddEmpLGA option:selected").val();
                var cNatureOfBusiness = $("#ddNatureOfBusiness option:selected").val();
                var cDesignation = $("#txtDesignation").val();
                var cEmpStaffID = $("#txtEmpStaffID").val();
                var cOfficeNumber = $("#txtOfficeNumber").val();
                var cEmpMobileNoInt = $("#txtEmpMobileNoInt").val();
                var cEmpEmailAddress = $("#txtEmpEmailAddress").val();
                var cZipCode = $("#txtZipCode").val();
                var cDateOfFirstEmployment = $("#txtDateOfFirstEmployment").val();
                var cDateOfRetirement = $("#txtDateOfRetirement").val();
                var cDateOfCurrentAppointmentPublic = $("#txtDateOfCurrentAppointmentPublic").val();
                var cEmpServiceNo = $("#txtEmpServiceNo").val();    
                var cDateOfTransferOfService = $("#txtDateOfTransferOfService").val();
                var cEmpPOBox = $("#txtEmpPOBox").val();
                var cIPPIS = $("#ddIPPIS option:selected").val();
                var cDateJoinedIPPIS = $("#txtDateJoinedIPPIS").val();
                var cHarmonised2004 = $("#ddHarmonised2004 option:selected").val();
                var cGLJune2004 = $("#ddGLJune2004 option:selected").val();
                var cStep2004 = $("#ddStep2004 option:selected").val();
                var cHarmonised2007 = $("#ddHarmonised2007 option:selected").val();
                var cGLJune2007 = $("#ddGLJune2007 option:selected").val();
                var cStep2007 = $("#ddStep2007 option:selected").val();
                var cHarmonised2010 = $("#ddHarmonised2010 option:selected").val();
                var cGLJune2010 = $("#ddGLJune2010 option:selected").val();
                var cStep2010 = $("#ddStep2010 option:selected").val();

                var cNOKTitle = $("#ddNOKTitle option:selected").val();
                var cNOKGender = $("#ddNOKGender option:selected").val();
                var cNOKRelationship = $("#ddNOKRelationship option:selected").val();
                var cNOKSurname = $("#txtNOKSurname").val();
                var cNOKFirstName = $("#txtNOKFirstName").val();
                var cNOKMiddleName = $("#txtNOKMiddleName").val();
                var cNOKHouseNo = $("#txtNOKHouseNo").val();
                var cNOKStreetAddress = $("#txtNOKStreetAddress").val();
                var cNOKTown = $("#txtNOKCity").val();
                var cNOKCountry = $("#ddNOKCountry option:selected").val();
                var cNOKStates = $("#ddNOKStates option:selected").val();
                var cNOKLGAs = $("#ddNOKLGAs option:selected").val();
                var cNOKMobileNo = $("#txtNOKMobileNo").val();
                var cNOKMobileNoInt = $("#txtNOKMobileNoInt").val();
                var cNOKEmail = $("#txtNOKEmail").val();
                var cNOKPOBox = $("#txtNOKPOBox").val();
                var cNOKZipCode = $("#txtNOKZipCode").val();

                var cBENTitle = $("#ddBENTitle option:selected").val();
                var cBENGender = $("#ddBENGender option:selected").val();
                var cBENRelationship = $("#ddBENRelationship option:selected").val();
                var cBENSurname = $("#txtBENSurname").val();
                var cBENFirstName = $("#txtBENFirstName").val();
                var cBENMiddleName = $("#txtBENMiddleName").val();
                var cBENHouseNo = $("#txtBENHouseNo").val();
                var cBENStreetAddress = $("#txtBENStreetAddress").val();
                var cBENTown = $("#txtBENCity").val();
                var cBENCountry = $("#ddBENCountry option:selected").val();
                var cBENStates = $("#ddBENStates option:selected").val();
                var cBENLGAs = $("#ddBENLGAs option:selected").val();
                var cBENMobileNo = $("#txtBENMobileNo").val();
                var cBENMobileNoInt = $("#txtBENMobileNoInt").val();
                var cBENEmail = $("#txtBENEmail").val();
                var cBENPOBox = $("#txtBENPOBox").val();
                var cBENZipCode = $("#txtBENZipCode").val();


                var cSignature = $("#txtSignatureCert").val();
                var cSignatureStatus = $("#txtSignatureStatus").val();

                var cDOBCert = $("#txtDOBCert").val();
                var cDOBCertStatus = $("#txtDOBCertStatus").val();

                var cPassportImage = $("#txtPassportImage").val();
                var cPassportImageStatus = $("#txtPassportImageStatus").val();

                var cPOI = $("#txtPOI").val();
                var cPOIStatus = $("#txtPOIStatus").val();

                var cPOA = $("#txtPOA").val();
                var cPOAStatus = $("#txtPOAStatus").val();

                var cNoSignature = $("#txtNoSignatureCert").val();
                var cNoSignatureStatus = $("#txtNoSignatureStatus").val();

                var cEvidenceOfEmploymentImage = $("#txtEvidenceOfEmploymentImage").val();
                var cEmpImageStatus = $("#txtEmpImageStatus").val();

                $("#ddTitleData").html('<strong>Title:</strong> '+ cTitle);
                $("#txtSurnameData").html('<strong>Surname:</strong> '+ cSurname);
                $("#txtFirstNameData").html('<strong>First Name:</strong> ' + cFirstName);
                $("#txtMiddleNameData").html('<strong>Middle Name:</strong> '+ cMiddleName);
                $("#txtMothersMaidenNameData").html('<strong>Mother Maiden Name:</strong> '+ cMothersMaidenName);
                $("#ddGenderData").html('<strong>Gender:</strong> '+ cGender);
                $("#ddMaritalStatusData").html('<strong>Marital Status:</strong> '+ cMaritalStatus);
                $("#txtDOBData").html('<strong>Date of Birth:</strong> '+ cDOB);
                $("#txtPlaceOfBirthData").html('<strong>Place Of Birth:</strong> '+ cPlaceOfBirth);
                $("#ddCountryData").html('<strong>Country:</strong> '+ cCountry);
                $("#ddStatesData").html('<strong>State of Origin (if Nationality is Nigerian):</strong> '+ cStateOrigin);
                $("#ddLGAData").html('<strong>Local Government:</strong> '+ cLGA);
                $("#txtPassportData").html('<strong>International Passport Number:</strong> '+ cPassport);
                $("#txtBVNData").html('<strong>Bank Verification Number(BVN):</strong> '+ cBVN);
                $("#txtNINData").html('<strong>National Identity Number (NIN):</strong> '+ cNIN);

                if(cSignatureStatus==0 && cSignature=="") {
                    if (cNoSignatureStatus==0 && cNoSignature=="") {
                        $("#txtSignatureData").html('<strong>Signature:</strong> <span style="color: red;">Please Upload supporting document for finger physical impairment / Please Upload your signature</span>');
                    } else {
                        $("#txtSignatureData").html('');
                    }
                } else {
                    $("#txtSignatureData").html('');
                }

                if(cDOBCertStatus==0 && cDOBCert=="") {
                    $("#txtBirthCertData").html('<strong>Birth Certificate:</strong> <span style="color: red;">Please Upload your birth certificate</span>');
                } else {
                    $("#txtBirthCertData").html('');
                }

                if(cPassportImageStatus==0 && cPassportImage=="") {
                    $("#txtPassportImageData").html('<strong>Passport:</strong> <span style="color: red;">Please Upload your passport</span>');
                } else {
                    $("#txtPassportImageData").html('');
                }

                if(cPOIStatus==0 && cPOI=="") {
                    $("#txtPOIData").html('<strong>Proof of Identity:</strong> <span style="color: red;">Please Upload proof of identity</span>');
                } else {
                    $("#txtPOIData").html('');
                }

                if(cPOAStatus==0 && cPOA=="") {
                    $("#txtPOAData").html('<strong>Proof of Address:</strong> <span style="color: red;">Please Upload proof of address</span>');
                } else {
                    $("#txtPOAData").html('');
                }

                if(cEmpImageStatus==0 && cEvidenceOfEmploymentImage=="") {
                    $("#txtEmployerData").html('<strong>Proof of Address:</strong> <span style="color: red;">Please Upload proof of address</span>');
                } else {
                    $("#txtEmployerData").html('');
                }

                $("#txtHouseNoData").html('<strong>Building No./Name:</strong> '+ cHouseNo);
                $("#txtResidentialAddressData").html('<strong>Street Address:</strong> '+ cResidentialAddress);
                $("#txtResidentialCityData").html('<strong>Village/Town/City:</strong> '+ cResidentialCity);
                $("#ddResidentialCountryData").html('<strong>Country of Residence:</strong> '+ cResidentialCountry);
                $("#ddResidentialStateData").html('<strong>State of Residence:</strong> '+ cResidentialState);
                $("#txtResidentialLGAsData").html('<strong>Local Government Area:</strong> '+ cResidentialLGAs);
                $("#txtResidentialPOBOXData").html('<strong>P.O Box or PMB:</strong> '+ cResidentialPOBox);
                $("#txtResidentialZipCodeData").html('<strong>Zip Code:</strong> '+ cResidentialZipCode);
                $("#txtCorresHouseNoData").html('<strong>Correspondence Building No./Name:</strong> '+ cCorresHouseNo);
                $("#txtCorressAdderessData").html('<strong>Correspondence Street Address:</strong> '+ cCorressAddress);
                $("#txtCorressTownData").html('<strong>Correspondence Village/Town/City:</strong> '+ cCorressTown);
                $("#ddCorresCountryData").html('<strong>Correspondence Country:</strong> '+ cCorresCountry);
                $("#ddCorressStateData").html('<strong>Correspondence State:</strong> '+ cCorressState);
                $("#ddCorressLGAData").html('<strong>Correspondence LGA:</strong> '+ cCorressLGA);
                $("#txtCorressMobileData").html('<strong>Mobile:</strong> '+ cCorressMobile);
                $("#txtCorresMobileIntData").html('<strong>Mobile No (International Number):</strong> '+ cCorresMobileInt);
                $("#txtEmailData").html('<strong>Personal Email Address:</strong> '+ cEmail);
                $("#txtCorresPOBOXData").html('<strong>P.O Box or PMB:</strong> '+ cCorresPOBOX);
                $("#txtCorresZipData").html('<strong>Zip Code:</strong> '+ cCorresZip);

                $("#ddEmpSectorData").html('<strong>Sector Classification:</strong> '+ cEmpSector);
                $("#txtEmpNameData").html('<strong>Employment Name:</strong> '+ cEmpName);
                $("#txtEmpIDData").html('<strong>Employer Code:</strong> '+ cEmpID);
                $("#txtEmpHouseNoData").html('<strong>Building No./Name:</strong> '+ cEmpHouseNo);
                $("#txtEmpAddressData").html('<strong>Street Name:</strong> '+ cEmpAddress);
                $("#txtEmpCityData").html('<strong>Village/Town/City:</strong> '+ cEmpCity);
                $("#ddEmpCountryData").html('<strong>Country:</strong> '+ cEmpCountry);
                $("#ddEmpStatesData").html('<strong>State:</strong> '+ cEmpStates);
                $("#ddEmpLGAData").html('<strong>LGA:</strong> '+ cEmpLGA);
                $("#ddNatureOfBusinessData").html('<strong>Nature of Business:</strong> '+ cNatureOfBusiness);
                $("#txtDesignationData").html('<strong>Designation/Rank:</strong> '+ cDesignation);
                $("#txtEmpStaffIDData").html('<strong>Staff ID:</strong> '+ cEmpStaffID);
                $("#txtOfficeNumberData").html('<strong>Office Number:</strong> '+ cOfficeNumber);
                $("#txtEmpMobileNoIntData").html('<strong>Mobile No (International Number):</strong> '+ cEmpMobileNoInt);
                $("#txtEmpEmailAddressData").html('<strong>Office Email Address:</strong> '+ cEmpEmailAddress);
                $("#txtZipCodeData").html('<strong>Zip Code:</strong> '+ cZipCode);
                $("#txtDateOfFirstEmploymentData").html('<strong>Date of First Employment:</strong> '+ cDateOfFirstEmployment);
                $("#txtDateOfRetirementData").html('<strong>Expected Date of Retirement:</strong> '+ cDateOfRetirement);
                $("#txtDateOfCurrentAppointmentPublicData").html('<strong>Date of Current Appointment (Public Sector Only):</strong> '+ cDateOfCurrentAppointmentPublic);
                $("#txtEmpServiceNoData").html('<strong>IPPIS No./Oracle No/Service ID:</strong> '+ cEmpServiceNo);        
                $("#txtDateOfTransferOfServiceData").html('<strong>Date of Transfer of Service:</strong> '+ cDateOfTransferOfService);
                $("#txtEmpPOBoxData").html('<strong>P.O Box or PMB:</strong> '+ cEmpPOBox);
                $("#ddIPPISData").html('<strong>Member of IPPIS:</strong> '+ cIPPIS);
                $("#txtDateJoinedIPPISData").html('<strong>Date of Joined IPPIS:</strong> '+ cDateJoinedIPPIS);
                $("#ddHarmonised2004Data").html('<strong>Harmonised Salary Structure as at 2004:</strong> '+ cHarmonised2004);
                $("#ddGLJune2004Data").html('<strong>GL as at June 2004:</strong> '+ cGLJune2004);
                $("#ddStep2004Data").html('<strong>Step as at June 2004:</strong> '+ cStep2004);
                $("#ddHarmonised2007Data").html('<strong>Consolidated Salary Structure as at 2007:</strong> '+ cHarmonised2007);
                $("#ddGLJune2007Data").html('<strong>GL as at June 2007:</strong> '+ cGLJune2007);
                $("#ddStep2007Data").html('<strong>Step as at June 2007:</strong> '+ cStep2007);
                $("#ddHarmonised2010Data").html('<strong>Consolidated Salary Structure as at 2010:</strong> '+ cHarmonised2010);
                $("#ddGLJune2010Data").html('<strong>GL as at June 2010:</strong> '+ cGLJune2010);
                $("#ddStep2010Data").html('<strong>Step as at June 2010:</strong> '+ cStep2010);

                $("#ddNOKTitleData").html('<strong>Title:</strong> '+ cNOKTitle);
                $("#ddNOKGenderData").html('<strong>Gender:</strong> '+ cNOKGender);
                $("#ddNOKRelationshipData").html('<strong>Relationship:</strong> '+ cNOKRelationship);
                $("#txtNOKSurnameData").html('<strong>Surname:</strong> '+ cNOKSurname);
                $("#txtNOKFirstNameData").html('<strong>First Name:</strong> '+ cNOKFirstName);
                $("#txtNOKMiddleNameData").html('<strong>Middle Name:</strong> '+ cNOKMiddleName);
                $("#txtNOKHouseNoData").html('<strong>Building No./Name:</strong> '+ cNOKHouseNo);
                $("#txtNOKStreetAddressData").html('<strong>Street Address:</strong> '+ cNOKStreetAddress);
                $("#txtNOKCityData").html('<strong>Village/Town/City:</strong> '+ cNOKTown);
                $("#ddNOKCountryData").html('<strong>Country of Residence:</strong> '+ cNOKCountry);
                $("#ddNOKStatesData").html('<strong>State of Residence:</strong> '+ cNOKStates);
                $("#ddNOKLGAsData").html('<strong>Local Government Area:</strong> '+ cNOKLGAs);
                $("#txtNOKMobileNoData").html('<strong>Mobile No:</strong> '+ cNOKMobileNo);
                $("#txtNOKMobileNoIntData").html('<strong>Mobile No (International Number):</strong> '+ cNOKMobileNoInt);
                $("#txtNOKEmailData").html('<strong>Email Address:</strong> '+ cNOKEmail);
                $("#txtNOKPOBoxData").html('<strong>P.O Box or PMB:</strong> '+ cNOKPOBox);
                $("#txtNOKZipCodeData").html('<strong>Zip Code:</strong> '+ cNOKZipCode);

                $("#ddBENTitleData").html('<strong>Title:</strong> '+ cBENTitle);
                $("#ddBENGenderData").html('<strong>Gender:</strong> '+ cBENGender);
                $("#ddBENRelationshipData").html('<strong>Relationship:</strong> '+ cBENRelationship);
                $("#txtBENSurnameData").html('<strong>Surname:</strong> '+ cBENSurname);
                $("#txtBENFirstNameData").html('<strong>First Name:</strong> '+ cBENFirstName);
                $("#txtBENMiddleNameData").html('<strong>Middle Name:</strong> '+ cBENMiddleName);
                $("#txtBENHouseNoData").html('<strong>Building No./Name:</strong> '+ cBENHouseNo);
                $("#txtBENStreetAddressData").html('<strong>Street Address:</strong> '+ cBENStreetAddress);
                $("#txtBENCityData").html('<strong>Village/Town/City:</strong> '+ cBENTown);
                $("#ddBENCountryData").html('<strong>Country of Residence:</strong> '+ cBENCountry);
                $("#ddBENStatesData").html('<strong>State of Residence:</strong> '+ cBENStates);
                $("#ddBENLGAsData").html('<strong>Local Government Area:</strong> '+ cBENLGAs);
                $("#txtBENMobileNoData").html('<strong>Mobile No:</strong> '+ cBENMobileNo);
                $("#txtBENMobileNoIntData").html('<strong>Mobile No (International Number):</strong> '+ cBENMobileNoInt);
                $("#txtBENEmailData").html('<strong>Email Address:</strong> '+ cBENEmail);
                $("#txtBENPOBoxData").html('<strong>P.O Box or PMB:</strong> '+ cBENPOBox);
                $("#txtBENZipCodeData").html('<strong>Zip Code:</strong> '+ cBENZipCode);


                $("#section-4 .help-block.with-errors.mandatory-error").html('');
                $("#section-4").removeClass("open");$("#section-4").addClass("slide-left");
                $("#section-5").removeClass("slide-right");$("#section-5").addClass("open");
            }
        });


    } else { 
        $("#section-5 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}

function nextStep5c() {
    var cRecordID = $('#txtRecordID').val();
    var cNOKTitle = $("#ddNOKTitle option:selected").val();
    var cNOKGender = $("#ddNOKGender option:selected").val();
    var cNOKRelationship = $("#ddNOKRelationship option:selected").val();
    var cNOKSurname = $("#txtNOKSurname").val();
    var cNOKFirstName = $("#txtNOKFirstName").val();
    var cNOKMiddleName = $("#txtNOKMiddleName").val();
    var cNOKHouseNo = $("#txtNOKHouseNo").val();
    var cNOKStreetAddress = $("#txtNOKStreetAddress").val();
    var cNOKTown = $("#txtNOKCity").val();
    var cNOKCountry = $("#ddNOKCountry option:selected").val();
    var cNOKStates = $("#ddNOKStates option:selected").val();
    var cNOKLGAs = $("#ddNOKLGAs option:selected").val();
    var cNOKMobileNo = $("#txtNOKMobileNo").val();
    var cNOKMobileNoInt = $("#txtNOKMobileNoInt").val();
    var cNOKEmail = $("#txtNOKEmail").val();
    var cNOKPOBox = $("#txtNOKPOBox").val();
    var cNOKZipCode = $("#txtNOKZipCode").val();

    var cBENTitle = $("#ddBENTitle").val();
    var cBENGender = $("#ddBENGender").val();
    var cBENRelationship = $("#ddBENRelationship").val();
    var cBENSurname = $("#txtBENSurname").val();
    var cBENFirstName = $("#txtBENFirstName").val();
    var cBENMiddleName = $("#txtBENMiddleName").val();
    var cBENHouseNo = $("#txtBENHouseNo").val();
    var cBENStreetAddress = $("#txtBENStreetAddress").val();
    var cBENTown = $("#txtBENCity").val();
    var cBENCountry = $("#ddBENCountry").val();
    var cBENStates = $("#ddBENStates").val();
    var cBENLGAs = $("#ddBENLGAs").val();
    var cBENMobileNo = $("#txtBENMobileNo").val();
    var cBENMobileNoInt = $("#txtBENMobileNoInt").val();
    var cBENEmail = $("#txtBENEmail").val();
    var cBENPOBox = $("#txtBENPOBox").val();
    var cBENZipCode = $("#txtBENZipCode").val();

    if(cNOKTitle) 
        $(".validnoktittle .help-block.with-errors").html('');
    else 
        $(".validnoktittle .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select title</li></ul>');
    if(cNOKGender) 
        $(".validnokgender .help-block.with-errors").html('');
    else 
        $(".validnokgender .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select gender</li></ul>');
    if(cNOKRelationship) 
        $(".validnokrelationship .help-block.with-errors").html('');
    else 
        $(".validnokrelationship .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select relationship</li></ul>');
    if(cNOKSurname) 
        $(".validnoksurname .help-block.with-errors").html('');
    else 
        $(".validnoksurname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter surname</li></ul>');
    if(cNOKFirstName) 
        $(".validnokfirstname .help-block.with-errors").html('');
    else 
        $(".validnokfirstname .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter first name</li></ul>');
    if(cNOKHouseNo) 
        $(".validnokhouseno .help-block.with-errors").html('');
    else 
        $(".validnokhouseno .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter house number</li></ul>');
    if(cNOKStreetAddress) 
        $(".validnokstreetaddress .help-block.with-errors").html('');
    else 
        $(".validnokstreetaddress .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter street address</li></ul>');
    if(cNOKTown) 
        $(".validnokcity .help-block.with-errors").html('');
    else 
        $(".validnokcity .help-block.with-errors").html('<ul class="list-unstyled"><li>Please enter town</li></ul>');
    if(cNOKCountry) 
        $(".validnokcountry .help-block.with-errors").html('');
    else 
        $(".validnokcountry .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select country</li></ul>');
    if(cNOKStates) 
        $(".validnokstate .help-block.with-errors").html('');
    else 
        $(".validnokstate .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select state</li></ul>');
    if(cNOKLGAs) 
        $(".validnoklga .help-block.with-errors").html('');
    else 
        $(".validnoklga .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select local government area</li></ul>');
    if(cNOKMobileNo) 
        $(".validnokmobile .help-block.with-errors").html('');
    else 
        $(".validnokmobile .help-block.with-errors").html('<ul class="list-unstyled"><li>Please select mobile number</li></ul>');
 


    if(cNOKTitle&&cNOKGender&&cNOKRelationship&&cNOKSurname&&cNOKFirstName&&cNOKHouseNo&&cNOKStreetAddress&&cNOKTown&&cNOKCountry&&cNOKStates&&cNOKLGAs&&cNOKMobileNo) { 

        var dataString = 'id=' + cRecordID + '&noktitle=' + cNOKTitle + '&nokgender=' + cNOKGender + '&nokrelationship=' + cNOKRelationship + '&noksurname=' + cNOKSurname + '&nokfirstname=' + cNOKFirstName + '&nokmiddlename=' + cNOKMiddleName + 
        '&nokhouseno=' + cNOKHouseNo + '&nokstreetaddress=' + cNOKStreetAddress + '&nokcountry=' + cNOKCountry + '&nokstates=' + cNOKStates + '&noklgas=' + cNOKLGAs + '&noktown=' + cNOKTown + '&nokmobileno=' + 
        cNOKMobileNo + '&nokmobilenoint=' + cNOKMobileNoInt + '&nokemail=' + cNOKEmail + '&nokpobox=' + cNOKPOBox + '&nokzipcode=' + cNOKZipCode + '&bentitle=' + cBENTitle + '&bengender=' +
         cBENGender + '&benrelationship=' + cBENRelationship + '&bensurname=' + cBENSurname + '&benfirstname=' + cBENFirstName + '&benmiddlename=' + cBENMiddleName + '&benhouseno=' + cBENHouseNo + 
         '&benstreetaddress=' + cBENStreetAddress + '&bentown=' + cBENTown + '&bencountry=' + cBENCountry + '&benstate=' + cBENStates + '&benlga=' + cBENLGAs + '&benmobile=' + cBENMobileNo +
          '&benmobileint=' + cBENMobileNoInt + '&benemail=' + cBENEmail + '&benpobox=' + cBENPOBox + '&benzipcode=' + cBENZipCode;

        $.ajax({
            type: "POST",
            url: "ajax4.php",
            data: dataString,
            cache: false,
            success: function(html) {

                $("#section-5 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Record Saved Successfully</li></ul>');
            }
        });


    } else { 
        $("#section-5 .help-block.with-errors.mandatory-error").html('<ul class="list-unstyled"><li>Kindly ensure all mandatory fields are filled</li></ul>');
    }
}

function previousStep4() { 
    $("#section-4").removeClass("slide-left");
    $("#section-4").addClass("open");$("#section-5").removeClass("open");
    $("#section-5").addClass("slide-right");
}

function backToPage1() {
    $("#section-5").removeClass("slide-left");
    $("#section-5").addClass("open");$("#section-1").removeClass("open");
    $("#section-1").addClass("slide-right");
}