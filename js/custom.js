$(document).ready(function() {
  $("#associated_pin_msg").hide();
  var maxField = 5; // maximum field to add
  var i = 1;

  $("#add").click(function() {
    if (i < maxField) {
      i++; // increment field counter
      $("#dynamic_field_head").append(
        '<div class="col-sm-12" id="remove_am' +
          i +
          '"><div class="col-sm-5"><div class="form-group"><select class="form-control my_associated_pins" name="ddMyPFA[]" id="ddMyPFA' +
          i +
          '" title="" required="required"><option value="">--- Select PFA ---</option> <option value="AIICO PENSION MANAGERS LIMITED">AIICO PENSION MANAGERS LIMITED</option>  <option value="APT PENSION FUND MANAGERS LIMITED">APT PENSION FUND MANAGERS LIMITED</option>  <option value="ARM PENSION MANAGERS (PFA) LIMITED ">ARM PENSION MANAGERS (PFA) LIMITED </option>  <option value="AXA MASARD PENSION LIMITED">AXA MASARD PENSION LIMITED</option>  <option value="CRUSADERSTERLING PENSIONS LIMITED">CRUSADERSTERLING PENSIONS LIMITED</option>  <option value="FIDELITY PENSION MANAGERS LIMITED">FIDELITY PENSION MANAGERS LIMITED</option>  <option value="FIRST GUARANTEE PENSION LIMITED">FIRST GUARANTEE PENSION LIMITED</option>  <option value="FUTURE UNITY GLANVILLS PENSIONS LIMITED">FUTURE UNITY GLANVILLS PENSIONS LIMITED</option>  <option value="IEI-ANCHOR PENSION MANAGERS LIMITED">IEI-ANCHOR PENSION MANAGERS LIMITED</option>  <option value="IGI PENSION FUND MANAGERS LIMITED">IGI PENSION FUND MANAGERS LIMITED</option>  <option value="INVESTMENT ONE PENSION MANAGERS LIMITED">INVESTMENT ONE PENSION MANAGERS LIMITED</option>  <option value="LEADWAY PENSURE PFA LIMITED">LEADWAY PENSURE PFA LIMITED</option>  <option value="LEGACY PENSION MANAGERS LIMITED (PFA)">LEGACY PENSION MANAGERS LIMITED (PFA)</option>  <option value="NLPC PENSION FUND ADMINISTRATORS LIMITED">NLPC PENSION FUND ADMINISTRATORS LIMITED</option>  <option value="NPF PENSIONS LIMITED">NPF PENSIONS LIMITED</option>  <option value="OAK PENSIONS LIMITED">OAK PENSIONS LIMITED</option>  <option value="PENSIONS ALLIANCE LIMITED">PENSIONS ALLIANCE LIMITED</option>  <option value="PREMIUM PENSION LIMITED">PREMIUM PENSION LIMITED</option>  <option value="SIGMA PENSIONS LIMITED">SIGMA PENSIONS LIMITED</option>  <option value="STANBIC IBTC PENSION MANAGERS LIMITED">STANBIC IBTC PENSION MANAGERS LIMITED</option>  <option value="TRUSTFUND PENSIONS PLC">TRUSTFUND PENSIONS PLC</option>  </select><div class="input-group-icon"><i class="fa fa-book"></i></div></div></div><div class="col-sm-5"><div class="form-group"><input class="form-control my_associated_pins" name="txtAssociatedPin[]" id="txtAssociatedPin' +
          i +
          '" type="text" placeholder="RSA PIN" required="required"><div class="input-group-icon"><i class="fa fa-book"></i></div></div></div><div class="col-sm-2"><button type="button" name="remove" id="' +
          i +
          '" class="btn btn-custom btn_remove" style="background-color: #8c004a;">Remove</button></div></div>'
      );
    }
  });

  $(document).on("click", ".btn_remove", function() {
    var button_id = $(this).attr("id");
    $("#remove_am" + button_id + "").remove();
    i--; // decrement field counter
  });

  $("#txtHasAssociatedPin").click(function() {
    if ($(this).is(":checked")) {
      $("#modal-associated-pins").show();
    } else {
      $("#modal-associated-pins").hide();
    }
  });
});

$(document).ready(function() {
  $("#ddEmpSector")
    .change(function() {
      $(this)
        .find("option:selected")
        .each(function() {
          var optionValue = $(this).attr("value");
          if (optionValue == "PU" || optionValue == "ST") {
            //$(".box").not("." + optionValue).hide();
            //$("." + optionValue).show();
            $(".ippishidden").show();
            $(".employerdocumentshidden").show();
            $(".salarystructurehidden").show();
            $(".employerdatehidden").show();
          } else {
            $(".ippishidden").hide();
            $(".employerdocumentshidden").hide();
            $(".salarystructurehidden").hide();
            $(".employerdatehidden").hide();
          }
          /***$.ajax({
          type:'POST',
          url:'ajaxCountry.php',
          data:'emp_sector='+optionValue,
          success:function(html){
              $('#txtEmpNames').html(html);
          }

    });***/
        });
    })
    .change();

  $("#ddEmpSector")
    .change(function() {
      $(this)
        .find("option:selected")
        .each(function() {
          var optionValue = $(this).attr("value");
          if (optionValue == "IF") {
            $(".natureofbusinesshidden").show();
          } else {
            $(".natureofbusinesshidden").hide();
          }
        });
    })
    .change();

  $("#txtHasSignature").click(function() {
    if ($(this).is(":checked")) {
      $(".rbNoSignature").show();
      $(".rbYesSignature").hide();
    } else {
      $(".rbNoSignature").hide();
      $(".rbYesSignature").show();
    }
  });

  $("#ddCountry").on("change", function() {
    var countryID = $(this).val();
    if (countryID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "country_id=" + countryID,
        success: function(html) {
          $("#ddStates").html(html);
          if (countryID == "566") {
            $("#ddLGA").html('<option value="">--- Select LGA---</option>');
            $(".validpassportnumber").hide();
          } else {
            $("#ddLGA").html('<option value="FR">Foreigner</option>');
            $(".validpassportnumber").show();
          }
        }
      });
    } else {
      $("#ddStates").html('<option value="">Select country first</option>');
      $("#ddLGA").html('<option value="">Select state first</option>');
    }
  });

  $("#ddResidentialCountry").on("change", function() {
    var countryID = $(this).val();
    if (countryID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "country_id=" + countryID,
        success: function(html) {
          $("#ddResidentialState").html(html);
          if (countryID == "566") {
            $("#txtResidentialLGAs").html(
              '<option value="">--- Select LGA---</option>'
            );
          } else {
            $("#txtResidentialLGAs").html(
              '<option value="FR">Foreigner</option>'
            );
          }
        }
      });
    } else {
      $("#ddResidentialState").html(
        '<option value="">Select country first</option>'
      );
      $("#txtResidentialLGAs").html(
        '<option value="">Select state first</option>'
      );
    }
  });

  $("#ddEmpCountry").on("change", function() {
    var countryID = $(this).val();
    if (countryID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "country_id=" + countryID,
        success: function(html) {
          $("#ddEmpStates").html(html);
          if (countryID == "566") {
            $("#ddEmpLGA").html('<option value="">--- Select LGA---</option>');
          } else {
            $("#ddEmpLGA").html('<option value="FR">Foreigner</option>');
          }
        }
      });
    } else {
      $("#ddEmpStates").html('<option value="">Select country first</option>');
      $("#ddEmpLGA").html('<option value="">Select state first</option>');
    }
  });

  $("#ddNOKCountry").on("change", function() {
    var countryID = $(this).val();
    if (countryID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "country_id=" + countryID,
        success: function(html) {
          $("#ddNOKStates").html(html);
          if (countryID == "566") {
            $("#ddNOKLGAs").html('<option value="">--- Select LGA---</option>');
          } else {
            $("#ddNOKLGAs").html('<option value="FR">Foreigner</option>');
          }
        }
      });
    } else {
      $("#ddNOKStates").html('<option value="">Select country first</option>');
      $("#ddNOKLGAs").html('<option value="">Select state first</option>');
    }
  });

  $("#ddBENCountry").on("change", function() {
    var countryID = $(this).val();
    if (countryID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "country_id=" + countryID,
        success: function(html) {
          $("#ddBENStates").html(html);
          if (countryID == "566") {
            $("#ddBENLGAs").html('<option value="">--- Select LGA---</option>');
          } else {
            $("#ddBENLGAs").html('<option value="FR">Foreigner</option>');
          }
        }
      });
    } else {
      $("#ddBENStates").html('<option value="">Select country first</option>');
      $("#ddBENLGAs").html('<option value="">Select state first</option>');
    }
  });

  $("#ddCorresCountry").on("change", function() {
    var countryID = $(this).val();
    if (countryID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "country_id=" + countryID,
        success: function(html) {
          $("#ddCorressState").html(html);
          if (countryID == "566") {
            $("#ddCorressLGA").html(
              '<option value="">--- Select LGA---</option>'
            );
          } else {
            $("#ddCorressLGA").html('<option value="FR">Foreigner</option>');
          }
        }
      });
    } else {
      $("#ddCorressState").html(
        '<option value="">Select country first</option>'
      );
      $("#ddCorressLGA").html('<option value="">Select state first</option>');
    }
  });

  $("#ddStates").on("change", function() {
    var stateID = $(this).val();
    if (stateID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "state_id=" + stateID,
        success: function(html) {
          $("#ddLGA").html(html);
        }
      });
    } else {
      $("#ddLGA").html('<option value="">Select state first</option>');
    }
  });

  $("#ddResidentialState").on("change", function() {
    var stateID = $(this).val();
    if (stateID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "state_id=" + stateID,
        success: function(html) {
          $("#txtResidentialLGAs").html(html);
        }
      });
    } else {
      $("#txtResidentialLGAs").html(
        '<option value="">Select state first</option>'
      );
    }
  });

  $("#ddEmpStates").on("change", function() {
    var stateID = $(this).val();
    if (stateID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "state_id=" + stateID,
        success: function(html) {
          $("#ddEmpLGA").html(html);
        }
      });
    } else {
      $("#ddEmpLGA").html('<option value="">Select state first</option>');
    }
  });

  $("#ddNOKStates").on("change", function() {
    var stateID = $(this).val();
    if (stateID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "state_id=" + stateID,
        success: function(html) {
          $("#ddNOKLGAs").html(html);
        }
      });
    } else {
      $("#ddNOKLGAs").html('<option value="">Select state first</option>');
    }
  });

  $("#ddBENStates").on("change", function() {
    var stateID = $(this).val();
    if (stateID) {
      $.ajax({
        type: "POST",
        url: "ajaxCountry.php",
        data: "state_id=" + stateID,
        success: function(html) {
          $("#ddBENLGAs").html(html);
        }
      });
    } else {
      $("#ddBENLGAs").html('<option value="">Select state first</option>');
    }
  });
});
