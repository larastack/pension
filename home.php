
<?php 
session_start(); 
require 'config/Leadway.php';

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Leaway Pensure CFA Data Recapture</title>
        <link href="css_cfi/bootstrap.min.css" rel="stylesheet"/>
        <link href="css_cfi/font-awesome.min.css" rel="stylesheet"/>
        <link href="css_cfi/style.css" rel="stylesheet"/>
        <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet"/>
        <link href="css_cfi/sweetalert.css" rel="stylesheet"/>
        <style>
            .error{
  color: red;
  font-weight: bold
}
          .tt-menu {
    max-height: 250px;
    overflow-y: auto;
    width: 100%;
    margin: 5px 0;
    padding: 5px;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, 0.2);
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 1px;
    -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
    -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
    box-shadow: 0 5px 10px rgba(0,0,0,.2);
    
}
.tt-suggestion {
    padding: 1px 10px;
    font-size: 18px;
    line-height: 24px;
    border-bottom: 1px solid #eee;
    cursor: pointer;
}
.twitter-typeahead{
  width: 100%
}
        </style>

        <?php 
            if(!isset($_SESSION['pin'])){
                session_destroy();
        ?>
            <script type="text/javascript">
                window.location.href = '<?php echo 'index.php'; ?>';
            </script>
        <?php 
            }
        ?>
    </head>
    <body>
        
        <div class="container">
            <strong>  NOTE All fields marked with asteriks <span style="color:red;">(*)</span> are compulsory . </strong>
            <?php 
                $leadway->alert();
            ?>
            <form action="" method="post" id="pension_form" name="pension_form">  
                <div class="wizards">
                    <div class="progressbar">
                        <div class="progress-line" data-now-value="12.11" data-number-of-steps="5" style="width: 12.11%;"></div> <!-- 19.66% -->
                    </div>
                    <!--   <div class="form-wizard active">
                           <div class="wizard-icon"><i class="fa fa-file-text-o"></i></div>
                           <p>Certification</p>
                       </div>
                    -->
                    <div class="form-wizard active">
                        <div class="wizard-icon"><i class="fa fa-user"></i></div>
                        <p>Personal Data</p>
                    </div>
                    <div class="form-wizard">
                        <div class="wizard-icon"><i class="fa fa-key"></i></div>
                        <p>Contact Address</p>
                    </div>
                    <div class="form-wizard">
                        <div class="wizard-icon"><i class="fa fa-globe"></i></div>
                        <p>Employment Details</p>
                    </div>
                    <div class="form-wizard">
                        <div class="wizard-icon"><i class="fa fa-check-circle"></i></div>
                        <p>Next Of Kin</p>
                    </div>
                    <div class="form-wizard">
                        <div class="wizard-icon"><i class="fa fa-file-text-o"></i></div>
                        <p>Review </p>
                    </div>
                    <div class="form-wizard">
                        <div class="wizard-icon"><i class="fa fa-globe"></i></div>
                        <p>Certification</p>
                    </div> 
                </div>
                <fieldset>
                    <div class="form-row">
                       <div class="form-group col-md-3">
                           <label for="title">Title <span class="error">*</span></label> 
                             <select id="TitleId" class="form-control check_error">
                                <option value="">Choose... Title.</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                        <label for="surname">Surname<span class="error">*</span></label>
                        
                            <input type="text" class="form-control check_error" id="LastName" name="LastName" placeholder="Surname">
                            <input type="hidden" class="form-control" id="UserId" name="UserId">
                            <input type="hidden" class="form-control" id="AgentCode" name="AgentCode" value="000">
                        </div>
                         <div class="form-group col-md-3">
                        <label for="firstname">First Name<span class="error">*</span></label>
                          <input type="text" class="form-control check_error" id="FirstName" name="FirstName" placeholder="First Name">
                        </div>
                        <div class="form-group col-md-3">
                        <label for="middle">Middle Name<span class="error">*</span></label>
                          <input type="text" class="form-control check_error" id="OtherName" name="OtherName" placeholder="Other Name">
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="gender">Gender<span class="error">*</span></label> 
                            <select id="Sex" name="Sex" class="form-control check_error">
                                <option value="">Choose... Gender</option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="gender">Marital Status</label> 
                            <select id="MaritalStatus" name="MaritalStatus" class="form-control">
                            
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="maidenname">Maiden Name</label>
                            <input type="text" class="form-control" id="MaidenName" name="MaidenName" placeholder="Maiden Name">
                        </div>

                        <div class="form-group  col-md-3">
                            <label for="">Place of Birth</label>
                            <input type="text" placeholder="Place of Birth" name="PlaceOfBirth" class="form-control typeahead" id="PlaceOfBirth">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group  col-md-3">
                            <label for="">Date of Birth<span class="error">*</span></label>
                            <input type="text" placeholder="Date of Birth" name="DateOfBirth" class="form-control datepicker check_error" id="DateOfBirth">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Nationality<span class="error">*</span></label>
                            <select class="form-control check_error" name="Nationality" id="Nationality">
                                <option value="">Nationality</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">State of Origin<span class="error">*</span></label>
                            <select class="form-control check_error" name="StateOfOrigin" id="StateOfOrigin">
                                <option value="">State of Origin</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">LGA of Origin<span class="error">*</span></label>
                            <select class="form-control check_error" name="LGAOfOrigin" id="LGAOfOrigin">
                                <option value="">LGA of Origin</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="">OtherPfa</label>
                            <select class="form-control OtherPFA" name="OtherPFA" id="OtherPFA">
                                <option value="">Select A Pfa</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="">Other Pin</label>
                            <input type="text" class="form-control" id="OtherPencomPIN" name="OtherPencomPIN" placeholder="Other Pin">
                        </div>

                        

                        <div class="form-group col-md-3">
                            <label for="">Pin</label>
                            <input type="text" class="form-control" id="RSAPIN" name="RSAPIN" placeholder="Pin" readonly>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="">Physically challenged <span class="error">*</span></label>
                            <select class="form-control check_error" name="physically_challenged" id="physically_challenged">
                                <option value="">Select One</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>

                        
                        
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3">
                            <label for="">ID Card Type</label>
                            <select class="form-control" name="id_card_type" id="id_card_type">
                                <option value="">ID Card Type</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="">Valid ID</label>
                            <input type="text" class="form-control" id="id_cardno" name="id_cardno" placeholder="ID Card No">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="">BVN</label>
                            <input type="text" class="form-control check_error1" id="BVN" name="BVN" placeholder="BVN" maxlength="11" min="11" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">NIN</label>
                            <input type="text" class="form-control check_error1" id="NIN" name="NIN" placeholder="NIN" maxlength="15" min="11" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                        </div>

                        
                        
                        
                    </div>

                    <div class="wizard-buttons btn-group pull-right">
                        <!-- <button type="button" class="btn btn-next">Next</button> -->
                        <button class="btn btn-next pull-right" onClick="saveData()" type="button">Save and Proceed</button>
                        <button class="btn btn-primary pull-right" onClick="saveDataLater()" type="button">Save and Continue Later</button>
                    </div>
                </fieldset>

                <fieldset>
                    <div class="form-group row">
                        <label for="houseno" class="col-sm-2 col-form-label">House Number/Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="HouseNumberName" name="HouseNumberName" placeholder="House Number/Name">
                        </div>
                    </div>
                    <!--  <input type="text" name="houseno" placeholder="House Number/Name" /> -->
                    <div class="form-group row">
                        <label for="streetname" class="col-sm-2 col-form-label">Street Name </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="StreetName" name="StreetName" placeholder="Street Name">
                        </div>
                    </div>
                    <!-- <input type="text" name="street" placeholder="Street Name" /> -->
                    <div class="form-group row">
                        <label for="village" class="col-sm-2 col-form-label">Village/Town/City </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="VillageTownCity" name="VillageTownCity" placeholder="Village/Town/City">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-2">Country of Residence</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="CityCode" id="CityCode">
                                <option value="">Country Of Residence Code</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-2">State of Residence</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="StateCode" id="StateCode">
                                <option value="">State Of Residence Code</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="" class="col-sm-2">LGA of Residence</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="lga_res" id="lga_res">
                                <option value="">LGA code</option>
                            </select>
                        </div>
                    </div>

                    <!-- <div class="form-group row">
                        <label for="village" class="col-sm-2 col-form-label">ZipCode </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="ZipCode" name="ZipCode" placeholder="ZipCode">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="pobox" class="col-sm-2 col-form-label">P. O. Box</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="pobox" placeholder="P. O. Box">
                        </div>
                    </div> -->

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="mobileno">Mobile Number</label>
                            <input type="text" class="form-control" id="TelephoneNo" name="TelephoneNo" placeholder="Mobile Number ">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Phone Number </label>
                            <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="Phone Number ">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Email Address</label>
                            <input type="email" class="form-control check_error" id="Email" name="Email" placeholder="Email Address" required>
                        </div>
                    </div>
                    

                    <div class="wizard-buttons btn-group form-row pull-right">
                        <button type="button" class="btn btn-previous">Previous</button>
                        <!-- <button type="button" class="btn btn-next">Next</button> -->
                        <button type="button" onclick="saveContactData()" class="btn btn-next">Save and Proceed</button>
                        <button type="button" onclick="saveContactDataLater()" class="btn btn-primary">Save and Continue Later</button>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Please select sector</label>
                        <div class="col-sm-9">
                            <select name="SectorID" id="SectorID" class="form-control check_error">
                            <option value="">Select A Sector</option>
                            </select>
                        </div>  
                    </div>

                    <div id="private_div" class="hidden">
                        

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Employer Phone Number</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" name="OfficeTelNo" id="OfficeTelNo" />
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Official Email Address</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" name="OfficialEmailAddress" id="OfficialEmailAddress" />
                            </div>  
                        </div>
                    </div>
                    <!-- End Private Sector Div -->

                    <div id="federal_state_div" class="hidden">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Service Id No (Police and Paramilitary)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="ServiceId" id="ServiceId" />
                            </div>  
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Service No (Police and Paramilitary)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="ServiceNo" id="ServiceNo" />
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Date Of Current Appointment</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control datepicker" name="CurrentAppointmentDate" id="CurrentAppointmentDate" />
                            </div>  
                        </div>

                        

                        <div class="form-group row mb-5">
                            <label class="col-sm-3 col-form-label">Customer Under IPPIS</label>
                            <div class="col-sm-9">
                            <select name="customer_ippis" id="customer_ippis" class="form-control">
                                <option value="">Select Here</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                            </div>  
                        </div>
                        
                        <div class="form-group row hidden" id="ippis_div">
                            <div class="col-md-3">
                                <label class="">Date Employee Joined IPPIS</label>
                                <input type="text" class="form-control datepicker" name="IPPISJoinDate" id="IPPISJoinDate" />
                            </div>
                            
                            <div class="col-md-3">
                                <label class="">Employee IPPIS NO</label>
                                <input type="text" class="form-control" name="IPPISNO" id="IPPISNO" />
                            </div>
                        </div>

                        

                        <div class="form-group row" id="TextBoxDiv1">
                            <div class="col-md-3">
                                <label for="">Salary Structure</label>
                                <select name="salary_structure[]" id="salary_structure" class="form-control salary_structure">
                                    <option value="">Select An Salary Structure</option>
                                    <option value="Harmonized Salary Structure_2014">Harmonized Salary Structure As At 2014 (2014)</option>
                                    <option value="Consolidated Salary_2007">Consolidated Salary As At 2007 (2007)</option>
                                    <option value="Consolidated Salary_2010">Consolidated Salary As At 2010 (2010)</option>
                                    <option value="Harmonized Salary Structure_2013">Harmonized Salary Structure As At 2013 (2013) </option>
                                    <option value="Harmonized Salary Structure_2016">Harmonized Salary Structure As At 2016 (2016) </option>
                                    <option value="Current Salary Structure_2019">Current Salary Structure (2019)</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Grade Level</label>
                                <input type="text" class="form-control grade_level" name="grade_level[]" id="grade_level" placeholder="Grade Level" />
                            </div>
                            <div class="col-md-3">
                                <label for="">Grade Step</label>
                                <input type="text" class="form-control grade_step" name="grade_step[]" id="grade_step" placeholder="Grade Step" />
                            </div>
                            <div class="col-md-3">
                                <label for="">Salary</label>
                                <input type="number" class="form-control salary" name="salary[]" id="salary" placeholder="Salary" />
                            </div>
                        </div>
                        <div id="fieldwrapper"></div>



                        

                        <div class="form-row mb-5 btn-group">
                            <button id="add_btn" class="btn btn-primary"> Add </button>
                            <button id="clear_btn" class="btn btn-warning"> Clear </button>
                        </div>

                        <!-- <div class="row mt-5 mb-5 p-4">
                            <table class="table table-striped hidden" id="dynamic_table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Salary Structure</th>
                                        <th>Year</th>
                                        <th>Grade Level</th>
                                        <th>Step</th>
                                        <th>Salary</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div> -->

                    </div>  

                    

                    <div class="form-row row">
                        <div class="form-group col-md-3">
                            <label>Organisation Name <span class="error">*</span> </label>
                            <input type="text" name="org_name" id="org_name" class="form-control org_name" />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Employer ID No</label>
                            <input type="text" class="form-control" name="EmployerId" id="EmployerId" readonly />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Employer Code</label>
                            <input type="text" class="form-control" name="EmployerCode" id="EmployerCode" readonly />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Date Of Retirement</label>
                            <input type="text" class="form-control datepicker" name="retirement_date" id="retirement_date" />
                        </div>
                    </div>

                    <div class="form-row row">
                        <div class="form-group col-md-3">
                            <label for="">Monthly Total Emolnument</label>
                            <input type="text" class="form-control" name="MonthlyTotalEmolument" id="MonthlyTotalEmolument" />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">ER monthly contribution</label>
                            <input type="text" class="form-control" name="ExpectedERMonthlyContr" id="ExpectedERMonthlyContr" />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Expected EE Monthly Contribution</label>
                            <input type="text" class="form-control" name="ExpectedEEMonthlyContr" id="ExpectedEEMonthlyContr" />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">Voluntary Contribution</label>
                            <input type="text" class="form-control" name="VoluntryContribution" id="VoluntryContribution" />
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-md-3">
                            <label class="">Date of First Employment </label>
                            <input type="text" class="form-control datepicker" name="IPPISJoinDate" id="IPPISJoinDate" />
                                
                        </div>
                        <div class="col-md-3">
                            <label class="">Date of Appointment</label>
                            <input type="text" class="form-control datepicker" name="DateOfAppointment" id="DateOfAppointment" />
                                
                        </div>
                        <div class="col-md-3">
                            <label class="">Date of Transfer of Service</label>
                            <input type="text" class="form-control datepicker" name="TransferOfServiceDate" id="TransferOfServiceDate" />
                                
                        </div>

                        <div class="col-md-3">
                            <label class="">Date of Current Employment</label>
                            <input type="text" class="form-control datepicker" name="DateOfCurrentEmployment" id="DateOfCurrentEmployment" />
                                
                        </div>

                        
                    </div>    

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="">Industry</label>
                            <select class="form-control" name="Industry" id="Industry">
                                <option value="">Select Industry</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Qualification</label>
                            <select name="Qualification" id="Qualification" class="form-control">
                                <option>Select An qualification</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Occupation</label>
                            <select name="occupation" id="occupation" class="form-control">
                                <option>Select An Occupation</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Building No/Name</label>
                            <input type="text" class="form-control" id="EmployerBuildingNumberName" name="EmployerBuildingNumberName" placeholder="Building No/Name ">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="">Street Name</label>
                            <input type="text" class="form-control" id="StreetName" name="StreetName" placeholder="Street Name ">
                        </div>
                        <div class="col-md-4">
                            <label for="">Village/Town/City</label>
                            <input type="text" class="form-control" id="EmployerVillageTownCity" name="EmployerVillageTownCity" placeholder="Village/Town/City ">
                        </div>
                        <div class="col-md-4">
                            <label for="">House Number/Name</label>
                            <input type="text" class="form-control" id="EmployerBuildingNumberName" placeholder="House Number/Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="">P. O. Box</label>
                            <input type="text" class="form-control" id="pobox" name="pobox" placeholder="P. O. Box">
                        </div>
                        <div class="col-md-4">
                            <label for="">Mobile Number</label>
                            <input type="text" class="form-control" id="mobileno" placeholder="Mobile Number ">
                        </div>
                        
                        <div class="col-md-4">
                            <label for="">Designation / Rank</label>
                            <input type="text" class="form-control" name="Designation" id="Designation">
                        </div>
                    </div>
                    
                    <br/>
                    <div class="wizard-buttons btn-group pull-right">
                        <button type="button" class="btn btn-previous">Previous</button>
                        <!-- <button type="button" class="btn btn-next">Next</button> -->
                        <button type="button" onclick="saveEmploymentData();" class="btn btn-next">Save and Proceed</button>
                        <button type="button" onclick="saveEmploymentDataLater();" class="btn btn-primary">Save and Continue Later</button>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="title">Title</label> 
                            <select id="NOKTitle" name="NOKTitle" class="form-control">
                                <option value="">Select... Title</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="surname">Next Of Kin Surname<span class="error">*</span></label>
                            <input type="text" class="form-control check_error" id="NOKLastName" name="NOKLastName" placeholder="Next Of Kin Surname">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nokfirstname" >Next of Kin First Name<span class="error">*</span></label>
                            <input type="text" class="form-control check_error" id="NOKFirstName" placeholder="Next of KIn First Name">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nokmiddle" >Next of Kin  Middle Name<span class="error">*</span></label>
                            <input type="text" class="form-control check_error" id="NOKOtherNames" name="NOKOtherNames" placeholder="NOK Middle Name">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="gender">Gender <span class="error">*</span></label> 
                            <select class="form-control check_error" name="NOKSex" id="NOKSex" data-error="Please Select Gender">
                                <option value="">--- Select Your Gender* ---</option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Relationship</label> 
                            <select class="form-control check_error" name="NOKRelationship" id="NOKRelationship">
                                <option value="">Select Relationship</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="surname" >Next Of Kin Mobile Number <span class="error">*</span></label>
                            <input type="text" class="form-control check_error" id="NOKMobile" placeholder="Next Of Kin Mobile No">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="nokmiddle">Next of Kin  Phone Number</label>
                            <input type="text" class="form-control" id="NOKPhone" name="NOKPhone" placeholder="Phone Number">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="nokmiddle" >Next of Kin Email Address</label>
                            <input type="text" class="form-control" id="NOKEmail" name="NOKEmail" placeholder="Email Address">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="">Country <span class="error">*</span></label>
                            <select class="form-control check_error" name="NOKCountryCode" id="NOKCountryCode">
                                <option>Country Of Residence Code</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">State</label>
                            <select class="form-control" name="NOKStateCode" id="NOKStateCode">
                                <option>State Of Residence Code</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">LGA</label>
                            <select class="form-control" name="NOKCityCode" id="NOKCityCode">
                                <option>LGA Of Residence Code</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="">Village/Town/City</label>
                            <input type="text" class="form-control" id="NOKVillageTownCity" name="NOKVillageTownCity" placeholder="Village/Town/City ">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">House Number/Name</label>
                            <input type="text" class="form-control" id="NOKHouseNumberName" name="NOKHouseNumberName" placeholder="House Number/Name">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="">P. O. Box</label>
                            <input type="text" class="form-control" id="pobox" placeholder="P. O. Box">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="">Streetname</label>
                            <input type="text" class="form-control" id="NOKStreetName" name="NOKStreetName" placeholder="Streetname">
                        </div>
                    </div>

                    <div class="wizard-buttons btn-group pull-right">
                        <button type="button" class="btn btn-previous">Previous</button>
                        <button type="button" onclick="saveNokData();" class="btn btn-next">Save and Proceed</button>
                        <button type="button" onclick="saveNokDataLater();" class="btn btn-primary">Save and Continue Later</button>
                    </div>

                </fieldset>
                <fieldset>
                    <div class="row review-submit-section">
                        <!-- <div class="column col-sm-12 align-me">
                             <button onclick="myFunction();">Print</button>
                         </div> -->
                        <div id="print_review" class="col-sm-12">
                            <div class="column col-sm-12 text-center align-me">
                                <h5>Bio Data</h5>
                                <p id="title"></p>
                                <p id="lastname"></p>
                                <p id="firstname"></p>
                                <p id="othername"></p>
                                <p id="sex"></p>
                                <p id="marital_status"></p>
                                <p id="maiden_name"></p>
                                <p id="place_birth"></p>
                                <p id="date_birth"></p>
                                <p id="country"></p>
                                <p id="state_origin"></p>
                                <p id="lga_origin"></p>
                                <p id="other_pfa"></p>
                                <p id="other_pencom_pin"></p>
                                <p id="physically_challenged_view"></p>
                                <p id="rsa_pin"></p>
                                <p id="bvn"></p>
                                <p id="nin"></p>
                            </div>
                            <div class="column col-sm-12 text-center align-me">
                                <h5>Contact Address</h5>
                                <p id="house_number_name"></p>
                                <p id="street_name"></p>
                                <p id="village_town_city"></p>
                                <p id="country_residenece"></p>
                                <p id="state_residence"></p>
                                <p id="lga_residenece"></p>
                                <p id="mobile_number"></p>
                                <p id="phone_number"></p>
                                <p id="email_address"></p>
                            </div>
                            <div class="column col-sm-12 text-center align-me">
                                <h5>Employment Details</h5>
                                <p id="sector_id"></p>
                                <p id="public_sector"></p>
                                <p id="service_no"></p>
                                <p id="current_appointment_date"></p>
                                <p id="ippis_no"></p>
                                <p id="customer_ippis_cs"></p>
                                <p id="ippis_date"></p>
                                <p id="organisation_name"></p>
                                <p id="industry_name"></p>
                                <p id="employer_id"></p>
                                <p id="employer_code"></p>
                                <p id="date_retirement"></p>
                                <p id="date_first_emp"></p>
                                <p id="date_appointment"></p>

                                <p id="date_curent_emp"></p>
                                <p id="monthly_total_emolument"></p>
                                <p id="expected_ee_monthly_contr"></p>
                                <p id="expected_er_monthly_contr"></p>
                                
                                <p id="voluntry_contribution"></p>
                                <p id="qualification_emp"></p>
                                <p id="occupation_emp"></p>
                                <p id="emp_building_number_name"></p>
                                <p id="emp_streetname"></p>
                                <p id="emp_village"></p>
                                <p id="emp_pobox"></p>
                                
                                <p id="emp_mobileno"></p>
                                <p id="emp_email"></p>
                                <p id="emp_designation"></p>
                                <p id="date_of_current_emp"></p>
                                <p id="office_tel_no"></p>
                                <p id="official_email_address"></p>
                            </div>
                            <div class="column col-sm-12 text-center align-me">
                                <h5>Next of Kin</h5>
                                <p id="nok_title"></p>
                                <p id="nok_lastname"></p>
                                <p id="nok_firstname"></p>
                                <p id="nok_othername"></p>
                                <p id="nok_sex"></p>
                                <p id="nok_relationship"></p>
                                <p id="nok_mobile"></p>
                                <p id="nok_phone"></p>
                                <p id="nok_email"></p>
                                <p id="nok_country"></p>
                                <p id="nok_state"></p>
                                <p id="nok_lga"></p>
                                <p id="nok_village"></p>
                                <p id="nok_house_number"></p>
                                <p id="nok_pobox"></p>
                                <p id="nok_streetname"></p>
                            </div>
                        </div>
                    </div>

                    <div class="wizard-buttons btn-group pull-right">
                        <button type="button" class="btn btn-previous">Previous</button>
                        <button type="button" class="btn btn-next">Save and Proceed</button>
                    </div>

                    
                    

                </fieldset>
                <fieldset>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Profile Picture <span class="error">*</span>:</label>
                            <input type="file" accept="image/jpeg, image/png" id="uploadPic" name="uploadPic" onchange="uploadPPP(this);" >
                            <input type="hidden" name="profile_pic_ppp" id="profile_pic_ppp" />
                        </div>

                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Signature <span class="error">*</span>:</label>
                            <input type="file" accept="image/jpeg, image/png" id="uploadSignatureFile" name="uploadSignatureFile" onchange="uploadSignature(this)" />
                            <input type="hidden" name="txtSignature" id="txtSignature" />
                        </div>

                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Employer Letter:</label>
                            <input type="file" accept="image/jpeg, image/png" id="uploadIdCardFile" name="uploadIdCardFile" onchange="uploadIdCard(this)" />
                            <input type="hidden" name="txtidCard" id="txtidCard" />
                        </div>

                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Utility Bill:</label>
                            <input type="file" accept="image/jpeg, image/png" id="uploadUtilityBillFile" name="uploadUtilityBillFile" onchange="uploadUtilityBill(this)" />
                            <input type="hidden" name="txtBirthCertificate" id="txtBirthCertificate">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Means of ID <span class="error">*</span>:</label>
                            <input type="file" accept="image/jpeg, image/png" id="MeansOfID" name="MeansOfID" onchange="uploadMeansOfID(this);" >
                            <input type="hidden" name="txtMeansOfID" id="txtMeansOfID" />
                        </div>

                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Proof Of Residency <span class="error">*</span>:</label>
                            <input type="file" accept="image/jpeg, image/png" id="ProofOfResidency" name="ProofOfResidency" onchange="uploadProofOfResidency(this);" >
                            <input type="hidden" name="txtProofOfResidency" id="txtProofOfResidency" />
                        </div>
                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Physically Challenge Document <span class="error">*</span>:</label>
                            <input type="file" accept="image/jpeg, image/png" id="PhyChallengeDocument" name="PhyChallengeDocument" onchange="uploadPhyChallengeDocument(this);" >
                            <input type="hidden" name="txtPhyChallengeDocument" id="txtPhyChallengeDocument" />
                        </div>
                    </div>    

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Additional Document (1):</label>
                            <input type="file" id="uploadDoc1" name="picture" accept="image/jpeg, image/png" onChage="uploadDoc1(this)" />
                            <input type="hidden" name="txtDoc1" id="txtDoc1">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Additional Document (2):</label>

                            <input type="file" id="uploadDoc2" name="signature" accept="image/jpeg, image/png" onChage="uploadDoc2(this)" />
                            <input type="hidden" name="txtDoc2" id="txtDoc2">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Additional Document (3):</label>

                            <input type="file" id="uploadDoc3" name="signature" accept="image/jpeg, image/png" onChage="uploadDoc3(this)" />
                            <input type="hidden" name="txtDoc3" id="txtDoc3">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="avatar">Upload Additional Document (4):</label>

                            <input type="file" id="uploadDoc4" name="signature" accept="image/jpeg, image/png" onChage="uploadDoc4(this)" />
                            <input type="hidden" name="txtDoc4" id="txtDoc4">
                        </div>
                    </div>  

                    

                    <iframe src="license.txt"  width="200" height="200"></iframe>

                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="yes" id="agree_form"> I agree with this license
                    </label>

                    <div class="wizard-buttons">
                        <button type="button" class="btn btn-previous">Previous</button>
                        <button id="pension_submit" type="button" onclick="finalSubmit()" class="btn btn-primary">Submit Form</button>
                    </div>

                    
                    
                    
                </fieldset>
                
            </form>
            <div id="final_div" class="row hidden">
                <div class="col-sm-12">
                    <h2>Record Saved Successfully</h2>
                </div>
            </div>
        </div>

        <!-- Load Modal Here -->

        <div class="modal fade" id="intro_modal" tabindex="-1" role="dialog" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Data Capture</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>
                        Welcome to the Leadway Pensure Data Capture form please ensure you fill all fields correnctly
                    <h5/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>



        

        <script src="js_cfi/jquery.min.js"></script>
        <script src="http://malsup.github.io/jquery.blockUI.js"></script>
        <!-- jQuery Library -->
	    <!-- <script src="js_cfi/jquery-2.2.4.min.js"></script> -->
        <!-- Bootstrap Datepicker -->  
	<script src="js_cfi/bootstrap-datepicker.min.js"></script>
        
        <script src="js_cfi/popper.min.js"></script>
        <script src="js_cfi/bootstrap.min.js"></script>
        <script src="js_cfi/sweetalert.min.js"></script>
        <script src="js_cfi/script.js"></script>

        
        
        <script src="js_cfi/validator.min.js"></script>
        <script src="js_cfi/multi-step-form.js"></script>
        <script type="text/javascript" src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
        <script type="text/javascript" src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
        <script type="text/javascript">
            const pin = '<?php echo $_SESSION['pin'] ?>';
        </script>
        <script>
            function myFunction() {
                window.print();
                e.preventDefault();
            }
        </script>
        <script src="js_cfi/customcfi.js"></script>
        
	
	
    
    </body>
</html>
