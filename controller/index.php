<?php 
 if(isset($_POST['pin']) && isset($_POST['password']) ){
   $pin = $_POST['pin'];
   $password = $_POST['password'];

   if(empty($pin))$leadway->makeAlert('Please enter your pin', 'warning');
   if(empty($password))$leadway->makeAlert('Please enter your password', 'warning');

   if(!$leadway->countAlert()){
    	$_SESSION['pin'] = $pin;
		##Store the browser where the user is logging in
		$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		##Store the IP-Address the user is uing to log in
		$_SESSION['remote_ip'] = $_SERVER['REMOTE_ADDR'];

       header("Location: home.php");
       die();
		?>
		<script type="text/javascript">
			window.location.href = '<?php echo 'home.php'; ?>';
		</script>
	<?php
	}
	else{
		$leadway->makeAlert("Operation failed", "warning");
	}

	
	
	$leadway->pageReload();

 }
?>