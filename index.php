<?php 
session_start(); 
require 'config/Leadway.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
        <meta name="description" content="Leadway Pensure PFA">
        <meta name="keywords" content="Leadway Pensure PFA Data Recapture">
        <title>Leaway Pensure CFA Data Recapture</title>
        <!-- <link href="css/mainLayout.css" rel="stylesheet" type="text/css" />  -->
        <link href="css_cfi/bootstrap.min.css" rel="stylesheet"/>
        <link href="css_cfi/font-awesome.min.css" rel="stylesheet"/>
        <link href="css_cfi/style.css" rel="stylesheet"/>
        <!-- <link href="css_cfi/custom.css" rel="stylesheet"/> -->
        <!-- <link href="css_cfi/bootstrap-datepicker3.min.css" rel="stylesheet"/> -->
        <link href="css/bootstrap-datepicker3.min.css" rel="stylesheet"/>
        <!-- <script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script> -->
        
    </head>
    <body>

        

        <div class="container" style="margin:30px auto;">
            <div class="row">
                <div class="col-12 clearfix">
                    <?php 
                        $leadway->alert();
                        include 'controller/index.php';
                    ?>
                </div>
                <div class="col-md-4 offset-md-4">
                    <form action="" method="post" autocomplete="off">
                        <div class="form-group">
                            <label for="">Pin Number</label>
                            <input class="form-control" type="text" name="pin" id="pin" maxlength="15">
                        </div>
                        <div class="form-group">
                            <label for=""></label>
                            <input class="form-control" type="password" name="password" id="password">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>



        <!-- jQuery Library -->

        <script src="js_cfi/jquery.min.js"></script>

        <!-- <script src="js_cfi/jquery-ui.js"></script> -->
        <script src="js_cfi/popper.min.js"></script>
        <script src="js_cfi/bootstrap.min.js"></script>
        <script src="js_cfi/script.js"></script>
        <script src="js_cfi/bootstrap-datepicker.min.js"></script>
        <!-- Form validator Js -->
        <script src="js_cfi/validator.min.js"></script>
        <!-- plugin main Js -->
        

        
    </body>
</html>
