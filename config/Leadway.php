<?php 
require_once("constant.php");

class Leadway{
	private $dbx; //database dbxnection

	private $DB_HOST; //Database host e.g localhost

	private $DB_NAME; //Database name

	private $DB_USER; //Database username

	private $DB_PASS; //Database password
	
	function __construct(){

		// $this->dbconnect();

	}
	private function dbconnect(){

		$x = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);//mysqli

		/* check dbxnection */

		if(mysqli_connect_error()) {

			die('Please refresh.' );

		} else $this->dbx = $x;

		$this->dbquery("SET SESSION group_dbxcat_max_len = 10000000;");

	}
	public function cleanInput($string, $length = null, $html = false, $striptags = true)
	{
		$length = 0 + $length;

		if(!$html) return ($length > 0) ? substr(addslashes(trim(preg_replace('/<[^>]*>/', '', $string))),0,$length) : addslashes(trim(preg_replace('/<[^>]*>/', '', $string)));

		$allow  = "<b><h1><h2><h3><h4><h5><h6><br><br /><hr><hr /><em><strong><a><ul><ol><li><dl><dt><dd><table><tr><th><td><blockquote><address><div><p><span><i><u><s><sup><sub><style><tbody>";

		$string = utf8_decode(trim($string));// avoid unicode codec issues

		if($striptags) $string = strip_tags($string, $allow);

		$aDisabledAttributes = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavaible', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragdrop', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterupdate', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmoveout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');

		$string = str_ireplace($aDisabledAttributes,'x',$string);

		//$string = preg_replace('/<(.*?)>/ie', '<' . preg_replace(array('/javascript:[^\"\']*/i', "/(" . implode('|', $aDisabledAttributes) . ")[ \\t\\n]*=[ \\t\\n]*[\"\'][^\"\']*[\"\']/i", '/\s+/'), array('', '', ' '), stripslashes('\\1')) . '>', strip_tags($string, $allow));

		//remove javascript from tags

		while( preg_match("/<(.*)?javascript.*?\(.*?((?>[^()]+)|(?R)).*?\)?\)(.*)?>/i", $string))

		$string = preg_replace("/<(.*)?javascript.*?\(.*?((?>[^()]+)|(?R)).*?\)?\)(.*)?>/i", "<$1$3$4$5>", $string);
		// dump expressions from contibuted content

		$string = preg_replace("/:expression\(.*?((?>[^(.*?)]+)|(?R)).*?\)\)/i", "", $string);
		while( preg_match("/<(.*)?:expr.*?\(.*?((?>[^()]+)|(?R)).*?\)?\)(.*)?>/i", $string))

		$string = preg_replace("/<(.*)?:expr.*?\(.*?((?>[^()]+)|(?R)).*?\)?\)(.*)?>/i", "<$1$3$4$5>", $string);

		// convert HTML characters

		$string = str_replace("#", "#", htmlentities($string));
		$string = addslashes(str_replace("%", "%", $string));
		if($length > 0) $string = substr($string, 0, $length);

		return $string;

	}//end of cleanInput function

	public function dbquery ($sql=""){

		/* 	This function dbxnects and queries a database. It returns the query result identifier.	*/

		if(empty($sql)) return false;

		if(is_null($this->dbx)) $this->dbconnect(); //dbxnect to database

		

		return $this->dbx->query($this->cleanSQL($sql)); //mysqli

		 

	}//end function dbquery	

	######################################################################
	##	Function to save pending alerts to session						##
	######################################################################
	public function makeAlert($msg = "", $info=""){	
		if(empty($msg)) return;
		$_SESSION['messageToAlert'][$info][] = $msg;
	}//end function alert

	

    	

	######################################################################

	##	Function to count pending alerts to session						##

	######################################################################

	public function countAlert(){	

		return count($_SESSION['messageToAlert']);

	}//end function alert

		

	######################################################################

	##	Function to display all messages logged during page generation	##

	######################################################################

	

	public function alert(){		
		if(!isset($_SESSION['messageToAlert'])) $_SESSION['messageToAlert'] = array();
			
		if(count($_SESSION['messageToAlert']) > 0){
			
			$x = $_SESSION['messageToAlert'];
			//$keys = array_keys($_SESSION['messageToAlert']);
									
			//remove duplicates
			//$x = array_unique($x);
			?>
            <?php 
            //print_r($x);
            $message = '';
			foreach ($x as $error_message) {
				# code...
				foreach($error_message as $e){
					$message .= $e;
					$message .= '<br/>';
				}				
			}
			echo '<div id="errorMessage" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content p-0 b-0">
                            <div class="panel panel-color panel-success">
                                <div class="panel-heading">
                                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 class="panel-title">Notification</h3>
                                </div>
                                <div class="panel-body">'.
									$message.'
                                </div>
                                
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->';
				foreach ($x as $key => $value) {
					# code...
					foreach($value as $v){
						if($key !== '')$alert = 'alert-'.$key;
						else $alert = '';
						echo '<div class="alert '. $alert.'" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="color:#fff;opacity: 1;">&times</button>
						'.$v.'</div>';
					}
					
				}
		}
		$_SESSION['messageToAlert'] = array();
	}//end function alert

	public function pageReload() {
	   ob_end_clean();
		die('<meta http-equiv="refresh" content="0"/> <script type="text/javascript">window.location.href=window.location.href;</script>');

	}//end function pageReload
  
}

$leadway = new Leadway();